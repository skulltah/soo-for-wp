﻿using Snoo.Common;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Snoo.Reddit;
using Newtonsoft.Json;
using Windows.UI.Popups;
using Snoo.Reddit.DataTypes.Prefixes;
using Snoo.Reddit.DataTypes;
using System.Diagnostics;
using System.Threading.Tasks;
using Snoo.Pages;
using System.ComponentModel;

using Snoo;

// The Universal Hub Application project template is documented at http://go.microsoft.com/fwlink/?LinkID=391955

namespace Snoo
{
    public class HubPageData : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }


        Theme _theme;
        public Theme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                NotifyPropertyChanged("Theme");
            }
        }

        Subreddits _subreddits;
        public Subreddits Subreddits
        {
            get
            {
                return _subreddits;
            }
            set
            {
                _subreddits = value;
                NotifyPropertyChanged("Subreddits");
            }
        }

        List<t5_> _recentSubreddits;
        public List<t5_> RecentSubreddits
        {
            get
            {
                return _recentSubreddits;
            }
            set
            {
                _recentSubreddits = value;
                NotifyPropertyChanged("RecentSubreddits");
            }
        }
    }

    /// <summary>
    /// A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class HubPage : Page
    {
        private readonly NavigationHelper navigationHelper;

        private HubPageData pageData = new HubPageData();

        public HubPage()
        {
            this.InitializeComponent();

            StatusBar statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
            statusBar.HideAsync();

            this.DataContext = pageData;


            // Hub is only supported in Portrait orientation
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;

            this.NavigationCacheMode = NavigationCacheMode.Required;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            pageData.Theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")));

            if (e.PageState != null && e.PageState.ContainsKey("reddits"))
            {
                pageData.Subreddits = e.PageState["reddits"] as Subreddits;
                //this.DataContext = this;
                //HubSection3.DataContext = OfflineReddit.GetRecentSubreddits();
            }
            else
            {
                await LoadData();

                if (LoginHelper.IsLoggedIn())
                    signInButton.Label = "sign out";
                else
                {
                    try
                    {
                        await Task.Delay(2000);

                        var menu = FindControl<ListView>(HubSection2, "menuListView");
                        menu.Items.RemoveAt(0);
                        menu.Items.RemoveAt(0);
                        //menu.Items.RemoveAt(0);
                    }
                    catch { }
                }
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            e.PageState.Add("reddits", HubSection1.DataContext);
        }

        private List<Control> AllChildren(DependencyObject parent)
        {
            var _List = new List<Control>();
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var _Child = VisualTreeHelper.GetChild(parent, i);
                if (_Child is Control)
                {
                    _List.Add(_Child as Control);
                }
                _List.AddRange(AllChildren(_Child));
            }
            return _List;
        }


        private T FindControl<T>(DependencyObject parentContainer, string controlName)
        {
            var childControls = AllChildren(parentContainer);
            var control = childControls.OfType<Control>().Where(x => x.Name.Equals(controlName)).Cast<T>().First();
            return control;
        }

        private async Task LoadData()
        {
            loadingControl.Show();

            Subreddits reddits = null;

            try
            {
                reddits = await OfflineReddit.GetSubreddits();
                if (reddits == null)
                    reddits = await Reddit.Reddit.GetSubreddits(100);
            }
            catch { }

            if (reddits != null)
            {
                pageData.Subreddits = reddits;

                OfflineReddit.SetSubreddits(reddits);
            }
            else
            {
                LoadSignedOutData();
            }

            pageData.RecentSubreddits = await OfflineReddit.GetRecentSubreddits();

            loadingControl.Hide();
        }

        private async void LoadSignedOutData()
        {
            Subreddits reddits = null;

            try
            {
                reddits = await OfflineReddit.GetSubreddits();
                if (reddits == null)
                    reddits = await Reddit.Reddit.GetDefaultSubreddits();
            }
            catch { }

            if (reddits != null)
            {
                pageData.Subreddits = reddits;

                OfflineReddit.SetSubreddits(reddits);
            }

            pageData.RecentSubreddits = await OfflineReddit.GetRecentSubreddits();
        }

        private async void GroupSection_ItemClick(object sender, ItemClickEventArgs e)
        {
            var subreddit = (Subreddits.Child)e.ClickedItem;

            try
            {
                pageData.RecentSubreddits = await OfflineReddit.AddRecentSubreddit(subreddit.Data);
            }
            finally
            {
                this.Frame.Navigate(typeof(SubredditPage), subreddit.Data);
            }
        }

        private async void ListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var subreddit = (t5_)e.ClickedItem;

            try
            {
                pageData.RecentSubreddits = await OfflineReddit.AddRecentSubreddit(subreddit);
            }
            finally
            {
                this.Frame.Navigate(typeof(SubredditPage), subreddit);
            }
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        #region AppBar

        private void settingsButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SettingsPage));
        }

        private async void signInButton_Click(object sender, RoutedEventArgs e)
        {
            loadingControl.Show();

            switch (signInButton.Label)
            {
                case "sign in":
                    var dialog0 = new LoginDialog();
                    dialog0.PrimaryButtonClick += dialog0_PrimaryButtonClick;
                    var result = await dialog0.ShowAsync();
                    
                    loadingControl.Show();

                    if (Helpers.General.StringToBool(Settings.GetSetting("NeedsToRegister") as string))
                    {
                        var dialog = new RegisterDialog();
                        await dialog.ShowAsync();
                        Settings.SetSetting("NeedsToRegister", "false");

                        await Task.Delay(500);

                        var cIden = Settings.GetSetting("CaptchaIden") as string;
                        if (cIden != null && cIden != "")
                        {
                            Debug.WriteLine(Settings.GetSetting("RegisterError") as string);
                            var msg = new MessageDialog(Settings.GetSetting("RegisterError") as string, Settings.GetSetting("RegisterErrorTitle") as string);
                            await msg.ShowAsync();
                        }
                        else
                        {
                            var reddits = await Reddit.Reddit.GetSubreddits(100);

                            OfflineReddit.SetSubreddits(reddits);

                            HubSection1.DataContext = reddits;

                            signInButton.Label = "sign out";
                        }
                    }

                    break;
                case "sign out":
                    var dialog2 = new MessageDialog("You will have to sign in again to use all the features this app has to offer.", "Are you sure?");

                    //OK Button
                    UICommand okBtn = new UICommand("sign out");
                    okBtn.Invoked = OkBtnClick;
                    dialog2.Commands.Add(okBtn);

                    //Cancel Button
                    UICommand cancelBtn = new UICommand("cancel");
                    dialog2.Commands.Add(cancelBtn);

                    await dialog2.ShowAsync();

                    break;
            }

            loadingControl.Hide();
        }

        async void dialog0_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            await Task.Delay(200);

            loadingControl.Show();

            await Task.Delay(5000);

            var reddits = await Reddit.Reddit.GetSubreddits(100);

            if (reddits == null)
            {
                var dialog = new MessageDialog("Couldn't verify login details.", "Error");
                await dialog.ShowAsync();

                loadingControl.Hide();
                return;
            }
            OfflineReddit.SetSubreddits(reddits);

            HubSection1.DataContext = reddits;

            signInButton.Label = "sign out";

            loadingControl.Hide();
        }

        private void OkBtnClick(IUICommand command)
        {
            LoginHelper.Logout();

            signInButton.Label = "sign in";

            HubSection1.DataContext = null;

            LoadSignedOutData();
        }

        private async void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            loadingControl.Show();

            Subreddits reddits = null;

            if (reddits == null)
                reddits = await Reddit.Reddit.GetSubreddits(100);

            if (reddits != null)
            {
                pageData.Subreddits = reddits;

                OfflineReddit.SetSubreddits(reddits);
            }
            else
            {
                if (reddits == null)
                    reddits = await Reddit.Reddit.GetDefaultSubreddits();

                if (reddits != null)
                {
                    pageData.Subreddits = reddits;

                    OfflineReddit.SetSubreddits(reddits);
                }

                pageData.RecentSubreddits = await OfflineReddit.GetRecentSubreddits();
            }

            loadingControl.Hide();
        }

        private void findButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SearchPage));
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SubmitPostPage), "");
        }

        #endregion

        private async void frontPageButton_Click(object sender, RoutedEventArgs e)
        {
            var subreddit = new t5_()
            {
                URL = "/",
                PublicDescription = "front page"
            };

            try
            {
                pageData.RecentSubreddits = await OfflineReddit.AddRecentSubreddit(subreddit);
            }
            finally
            {
                this.Frame.Navigate(typeof(SubredditPage), subreddit);
            }
        }

        private async void allButton_Click(object sender, RoutedEventArgs e)
        {
            var subreddit = new t5_()
            {
                URL = "/r/all/",
                PublicDescription = "all"
            };

            try
            {
                pageData.RecentSubreddits = await OfflineReddit.AddRecentSubreddit(subreddit);
            }
            finally
            {
                this.Frame.Navigate(typeof(SubredditPage), subreddit);
            }
        }

        private void StackPanel_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ProfilePage), LoginHelper.GetUsername());
        }

        private void StackPanel_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SubscriptionsPage));
        }

        private void StackPanel_Tapped_2(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SettingsPage));
        }

        t5_ subreddit { get; set; }
        private async void MenuFlyoutItem_Click(object sender, RoutedEventArgs e)
        {
            loadingControl.Show();

            await Reddit.Reddit.Unsubscribe(subreddit.Name);

            pageData.Subreddits = await Reddit.Reddit.GetSubreddits(100);

            loadingControl.Hide();
        }

        private void StackPanel_Holding(object sender, HoldingRoutedEventArgs e)
        {
            FrameworkElement senderElement = sender as FrameworkElement;
            FlyoutBase flyoutBase = FlyoutBase.GetAttachedFlyout(senderElement);
            flyoutBase.ShowAt(senderElement);

            subreddit = senderElement.Tag as t5_;
        }

        private void StackPanel_Tapped_3(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(AboutPage));
        }
    }
}