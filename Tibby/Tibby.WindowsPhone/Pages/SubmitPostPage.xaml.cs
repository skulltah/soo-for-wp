﻿using Snoo.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;
using System.Diagnostics;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Snoo.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SubmitPostPage : Page
    {
        private NavigationHelper navigationHelper;
        private BasicPageData pageData = new BasicPageData();

        private string iden = "";
        private string subreddit = "";

        public SubmitPostPage()
        {
            this.InitializeComponent();

            this.DataContext = pageData;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            pageData.Theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")));
            var hexToColourConverter = new HexToColorConverter();
            this.Background = (SolidColorBrush)hexToColourConverter.Convert(pageData.Theme.Background, typeof(SolidColorBrush), null, null);

            if (e.PageState != null)
            {
                titleTextBox.Text = e.PageState["title"] as string;
                urlTextBox.Text = e.PageState["url"] as string;
                selfTextBox.Text = e.PageState["self"] as string;
                isSelfCheckBox.IsChecked = e.PageState["isself"] as bool?;
                subreddit = e.PageState["subreddit"] as string;
            }
            else
            {
                subreddit = e.NavigationParameter as string;
            }

            LoadData();
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            if (e.PageState.ContainsKey("title"))
            {
                e.PageState["title"] = titleTextBox.Text;
                e.PageState["url"] = urlTextBox.Text;
                e.PageState["self"] = selfTextBox.Text;
                e.PageState["isself"] = isSelfCheckBox.IsChecked;
                e.PageState["subreddit"] = subreddit;
            }
            else
            {
                e.PageState.Add("title", titleTextBox.Text);
                e.PageState.Add("url", urlTextBox.Text);
                e.PageState.Add("self", selfTextBox.Text);
                e.PageState.Add("isself", isSelfCheckBox.IsChecked);
                e.PageState.Add("subreddit", subreddit);
            }
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async void LoadData()
        {
            if (subreddit != "")
                subredditComboBox.Visibility = Visibility.Collapsed;
            else
            {
                var subreddits = await Reddit.OfflineReddit.GetSubreddits();

                foreach (var sub in subreddits.Data.Children)
                {
                    var comboItem = new ComboBoxItem()
                    {
                        Content = sub.Data.URL,
                        Tag = sub.Data.DisplayName
                    };

                    subredditComboBox.Items.Add(comboItem);
                }
            }

            if ((await Reddit.Reddit.NeedsCaptcha()))
            {
                var cIden = Settings.GetSetting("CaptchaIden") as string;

                if (cIden != null && cIden != "")
                {
                    iden = cIden;
                    Settings.SetSetting("CaptchaIden", "");
                }
                else
                {
                    var captchaRequest = await Reddit.Reddit.GetNewCaptcha();
                    iden = captchaRequest.Json.Data.Iden;
                }

                var bitmap = await Reddit.Reddit.GetCaptcha(iden);

                captchaImage.Source = bitmap;

                captchaPanel.Visibility = Visibility.Visible;
            }
        }

        private async void Submit()
        {
            loadingControl.Show();

            if (subreddit == "" && subredditComboBox.SelectedIndex >= 0)
                subreddit = (subredditComboBox.SelectedItem as ComboBoxItem).Tag as string;

            Reddit.Reddit.PostKind kind = Reddit.Reddit.PostKind.link;

            if(isSelfCheckBox.IsChecked.Value)
                kind = Reddit.Reddit.PostKind.self;

            var dialog = new MessageDialog("Please fill in all required forms.", "Error");

            if (titleTextBox.Text == "")
                await dialog.ShowAsync();
            else if (subreddit == "")
                await dialog.ShowAsync();
            else
            {
                var result = await Reddit.Reddit.Post(captchaBox.Text, iden, kind, sendRepliesCheckBox.IsChecked.Value, subreddit, selfTextBox.Text, titleTextBox.Text, urlTextBox.Text);
                Debug.WriteLine(subreddit);
                if (result.Json.Errors.Length > 0)
                {
                    var cIden = "";
                    var errorTitle = "";
                    var error = "";

                    cIden = result.Json.Captcha;
                    errorTitle = result.Json.Errors[0][0];
                    error = result.Json.Errors[0][1];

                    var dialog1 = new MessageDialog(error, errorTitle);
                    await dialog1.ShowAsync();
                }

                selfTextBox.Text = "";
                titleTextBox.Text = "";
                urlTextBox.Text = "";

                this.Frame.GoBack();
            }

            loadingControl.Hide();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                selfTextBox.Visibility = Visibility.Visible;
                urlTextBox.Visibility = Visibility.Collapsed;
            }
            catch { }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                selfTextBox.Visibility = Visibility.Collapsed;
                urlTextBox.Visibility = Visibility.Visible;
            }
            catch { }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Submit();
        }
    }
}
