﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Snoo.Common;

using Snoo.Reddit.DataTypes;
using Snoo.Reddit.DataTypes.Prefixes;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.StartScreen;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Collections.ObjectModel;
using Windows.Phone.UI.Input;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Snoo.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SubredditPage : Page
    {
        t5_ subreddit = new t5_();
        private readonly NavigationHelper navigationHelper;

        private SubredditPageData pageData = new SubredditPageData();

        public SubredditPage()
        {
            this.DataContext = pageData;

            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        private async void LoadHotData()
        {
            sortButton.Visibility = Visibility.Collapsed;
            fullscreenButton.Visibility = Visibility.Visible;

            loadingControl.Show();

            pageData.Hot = (await Reddit.Reddit.GetPosts(subreddit.URL, Reddit.Reddit.SubredditListing.Hot, "", "", "", 0)).Data.Children;

            loadingControl.Hide();
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            pageData.Theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")));
            var hexToColourConverter = new HexToColorConverter();
            this.Background = (SolidColorBrush)hexToColourConverter.Convert(pageData.Theme.Background, typeof(SolidColorBrush), null, null);

            if (e.NavigationParameter != null)
                subreddit = e.NavigationParameter as t5_;

            if (e.PageState != null)
                subreddit = e.PageState["subreddit"] as t5_;

            if (subreddit.UserIsSubscriber)
                subscribeButton.Label = "unsubscribe";

            if (subreddit.URL == "/")
            {
                subscribeButton.IsEnabled = false;
                viewSidebarButton.IsEnabled = false;
                viewWikiButton.IsEnabled = false;
            }

            pivot.Title = subreddit.DisplayName;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            if (e.PageState == null || !e.PageState.ContainsKey("subreddit"))
                e.PageState.Add("subreddit", subreddit);
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);

            if (e.NavigationMode == NavigationMode.New)
            {
                LoadHotData();
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async void pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            pageData.Hot = null;
            pageData.New = null;
            pageData.Top = null;
            pageData.Controversial = null;

            await Task.Delay(200);

            switch ((pivot.SelectedItem as PivotItem).Header.ToString())
            {
                case "hot":
                    sortButton.Visibility = Visibility.Collapsed;
                    fullscreenButton.Visibility = Visibility.Visible;

                    loadingControl.Show();

                    pageData.Hot = (await Reddit.Reddit.GetPosts(subreddit.URL, Reddit.Reddit.SubredditListing.Hot, "", "", "", 0)).Data.Children;
                    break;
                case "new":
                    sortButton.Visibility = Visibility.Collapsed;
                    fullscreenButton.Visibility = Visibility.Collapsed;

                    await Task.Delay(1000);

                    loadingControl.Show();

                    pageData.New = (await Reddit.Reddit.GetPosts(subreddit.URL, Reddit.Reddit.SubredditListing.New, "", "", "", 0)).Data.Children;
                    break;
                case "top":
                    sortButton.Visibility = Visibility.Visible;
                    fullscreenButton.Visibility = Visibility.Collapsed;

                    await Task.Delay(1000);

                    loadingControl.Show();

                    pageData.Top = (await Reddit.Reddit.GetPosts(subreddit.URL, Reddit.Reddit.SubredditListing.Top, "", "", "", 0)).Data.Children;
                    break;
                case "controversial":
                    sortButton.Visibility = Visibility.Visible;
                    fullscreenButton.Visibility = Visibility.Collapsed;

                    await Task.Delay(1000);

                    loadingControl.Show();

                    pageData.Controversial = (await Reddit.Reddit.GetPosts(subreddit.URL, Reddit.Reddit.SubredditListing.Controversial, "", "", "", 0)).Data.Children;
                    break;
            }

            loadingControl.Hide();
        }

        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
             var post = ((sender as FrameworkElement).Parent as Grid).DataContext as t3_;

             var data = new PostPageData()
             {
                 Post = post
             };

             this.Frame.Navigate(typeof(PostPage), data);
        }

        private void Grid_Tapped2(object sender, TappedRoutedEventArgs e)
        {
            var post = ((sender as FrameworkElement).Parent as Grid).DataContext as t3_;

            ((Frame)Window.Current.Content).Navigate(typeof(CommentsPage), post.ID);
        }

        private async void subscribeButton_Click(object sender, RoutedEventArgs e)
        {
            // un/subscribe

            if (subscribeButton.Label == "subscribe")
            {
                loadingControl.Show();

                await Reddit.Reddit.Subscribe(subreddit.Name);

                subscribeButton.Label = "unsubscribe";

                loadingControl.Hide();
            }
            else
            {
                loadingControl.Show();

                await Reddit.Reddit.Unsubscribe(subreddit.Name);

                subscribeButton.Label = "subscribe";

                loadingControl.Hide();
            }
        }

        private async void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            // pin to start

            string displayName = subreddit.URL;
            string tileActivationArguments = displayName;

            SecondaryTile secondaryTile = new SecondaryTile(new Guid().ToString(),
                                                displayName,
                                                tileActivationArguments,
                                                new Uri("ms-appx:///Assets/Logo.scale-240.png", UriKind.Absolute),
                                                TileSize.Square150x150);

            await secondaryTile.RequestCreateForSelectionAsync(new Rect(), Windows.UI.Popups.Placement.Default);
        }

        private void viewSidebarButton_Click(object sender, RoutedEventArgs e)
        {
            // view sidebar
        }

        private void viewWikiButton_Click(object sender, RoutedEventArgs e)
        {
            // view wiki
        }

        private void AppBarButton_Click_2(object sender, RoutedEventArgs e)
        {
            // scroll view

            Database.SetValue("lastSubreddit", subreddit.URL);

            var newList = new ObservableCollection<Posts.Child>();

            int cnt = 0;
            foreach (var c in pageData.Hot)
            {
                if (cnt <= 5)
                    newList.Add(c);
                else
                    break;

                cnt++;
            }

            var data = new FlipViewPostData() { posts = newList };

            this.Frame.Navigate(typeof(FlipViewPage), data);
        }

        private void AppBarButton_Click_3(object sender, RoutedEventArgs e)
        {
            // new post

            this.Frame.Navigate(typeof(SubmitPostPage), subreddit.DisplayName);
        }

        private void AppBarButton_Click_4(object sender, RoutedEventArgs e)
        {
            // search

            this.Frame.Navigate(typeof(SearchPage), subreddit.DisplayName);
        }

        private async void sortButton_Click(object sender, RoutedEventArgs e)
        {
            // sort

            var flyout = new ListPickerFlyout() { Placement = FlyoutPlacementMode.Full };
            flyout.ItemsPicked += flyout_ItemsPicked;
            var items = new List<string>(new string[]{
                "Today",
                "This Hour",
                "This Week",
                "This Month",
                "This Year",
                "All Time"});
            flyout.ItemsSource = items;
            await flyout.ShowAtAsync(pivot.SelectedItem as PivotItem);
        }

        async void flyout_ItemsPicked(ListPickerFlyout sender, ItemsPickedEventArgs args)
        {
            loadingControl.Show();

            string sort = "";

            switch (args.AddedItems[0].ToString())
            {
                case "Today":
                    sort = "day";
                    break;
                case "This Hour":
                    sort = "hour";
                    break;
                case "This Week":
                    sort = "week";
                    break;
                case "This Month":
                    sort = "month";
                    break;
                case "This Year":
                    sort = "year";
                    break;
                case "All Time":
                    sort = "all";
                    break;
            }

            switch (pivot.SelectedIndex)
            {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    topListView.ItemsSource = (await Reddit.Reddit.GetPosts(subreddit.URL, Reddit.Reddit.SubredditListing.Top, sort, "", "", 0)).Data.Children;
                    break;
                case 3:
                    controversialListView.ItemsSource = (await Reddit.Reddit.GetPosts(subreddit.URL, Reddit.Reddit.SubredditListing.Controversial, sort, "", "", 0)).Data.Children;
                    break;
            }

            loadingControl.Hide();
        }

        bool isIncrementing = false;
        private async void hotScrollView_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            var result = hotScrollView.ScrollableHeight - hotScrollView.VerticalOffset;

            if (!isIncrementing && result <= 2500)
            {
                isIncrementing = true;

                var lastPost = pageData.Hot[pageData.Hot.Count - 1];

                var newData = (await Reddit.Reddit.GetPosts(subreddit.URL, Reddit.Reddit.SubredditListing.Hot, "", lastPost.Data.Name, "", pageData.Hot.Count)).Data.Children;

                foreach (var i in newData)
                    pageData.Hot.Add(i);

                isIncrementing = false;
            }
        }
    }
}
