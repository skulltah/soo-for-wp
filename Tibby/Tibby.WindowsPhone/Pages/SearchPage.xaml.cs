﻿using Snoo.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.System;
using System.ComponentModel;
using Snoo.Reddit.DataTypes;
using System.Threading.Tasks;
using Snoo.Reddit.DataTypes.Prefixes;
using System.Collections.ObjectModel;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Snoo.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SearchPage : Page
    {
        private NavigationHelper navigationHelper;
        private SearchPageData pageData = new SearchPageData();

        string _subreddit = "";

        public SearchPage()
        {
            this.InitializeComponent();

            this.DataContext = pageData;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            tb1.Focus(FocusState.Programmatic);
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            pageData.Theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")));
            var hexToColourConverter = new HexToColorConverter();
            this.Background = (SolidColorBrush)hexToColourConverter.Convert(pageData.Theme.Background, typeof(SolidColorBrush), null, null);

            if (e.PageState != null)
            {
                pageData = e.PageState["pageData"] as SearchPageData;
            }
            else
            {
                if (e.NavigationParameter != null)
                {
                    _subreddit = e.NavigationParameter as string;
                    pivot.Items.Remove(subredditsPivotItem);
                }
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            if (e.PageState == null || !e.PageState.ContainsKey("pageData"))
                e.PageState.Add("pageData", pageData);
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async void TextBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                loadingControl.Show();

                try
                {
                    if (_subreddit != "")
                    {
                        pageData.Posts = (await Reddit.Reddit.SearchSubreddit(_subreddit, "", "", 0, tb1.Text, Helpers.General.StringToBool(Settings.GetSetting("NSFW") as string))).Data.Children;
                    }
                    else
                    {
                        pageData.Subreddits = (await Reddit.Reddit.SearchSubreddits("", "", 0, tb1.Text)).Data.Children;
                        pageData.Posts = (await Reddit.Reddit.Search("", "", 0, tb1.Text, Helpers.General.StringToBool(Settings.GetSetting("NSFW") as string))).Data.Children;
                    }
                }
                catch { }

                loadingControl.Hide();
            }
        }

        private t3_ lastClickedPost { get; set; }
        private async void ListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                await Task.Delay(100);

                var post = lastClickedPost;

                var data = new PostPageData()
                {
                    Post = lastClickedPost
                };

                this.Frame.Navigate(typeof(PostPage), data);
            }
            catch { }
        }

        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            lastClickedPost = (sender as Grid).DataContext as t3_;
        }

        t5_ subreddit { get; set; }
        private async void ListView2_ItemClick(object sender, ItemClickEventArgs e)
        {
            await Task.Delay(100);
            this.Frame.Navigate(typeof(SubredditPage), subreddit);
        }

        private void StackPanel_Tapped(object sender, TappedRoutedEventArgs e)
        {
            FrameworkElement senderElement = sender as FrameworkElement;
            subreddit = senderElement.Tag as t5_;
        }

        private void MenuFlyoutItem_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SubredditPage), subreddit);
        }

        private void tb1_GotFocus(object sender, RoutedEventArgs e)
        {
            tb1.SelectAll();
        }
    }
}
