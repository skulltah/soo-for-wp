﻿using Snoo.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
#if WINDOWS_PHONE_APP
using Windows.Phone.UI.Input;
#endif
using Windows.UI.Popups;
using System.Diagnostics;
using System.ComponentModel;
using Windows.UI;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Snoo.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SettingsPage : Page
    {
        private NavigationHelper navigationHelper;

        private BasicPageData pageData = new BasicPageData();

        int theme = 0;

        public SettingsPage()
        {
            this.InitializeComponent();

            this.DataContext = pageData;

            switch (ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")))
            {
                case ThemeManager.Themes.Default:
                    themeComboBox.SelectedIndex = 0;
                    theme = 0;
                    break;
                case ThemeManager.Themes.AdriftinDreams:
                    themeComboBox.SelectedIndex = 1;
                    theme = 1;
                    break;
                case ThemeManager.Themes.Wordless:
                    themeComboBox.SelectedIndex = 2;
                    theme = 2;
                    break;
                case ThemeManager.Themes.DreamMagnet:
                    themeComboBox.SelectedIndex = 3;
                    theme = 3;
                    break;
                case ThemeManager.Themes.CoupDeGrace:
                    themeComboBox.SelectedIndex = 4;
                    theme = 4;
                    break;
                case ThemeManager.Themes.NewlyRisenMoon:
                    themeComboBox.SelectedIndex = 5;
                    theme = 5;
                    break;
                case ThemeManager.Themes.MachuPicchu:
                    themeComboBox.SelectedIndex = 6;
                    theme = 6;
                    break;
                case ThemeManager.Themes.GiantGoldfish:
                    themeComboBox.SelectedIndex = 7;
                    theme = 7;
                    break;
                case ThemeManager.Themes.ThoughtProvoking:
                    themeComboBox.SelectedIndex = 8;
                    theme = 8;
                    break;
                case ThemeManager.Themes.Terra:
                    themeComboBox.SelectedIndex = 9;
                    theme = 9;
                    break;
                case ThemeManager.Themes.mellonballsurprise:
                    themeComboBox.SelectedIndex = 10;
                    theme = 10;
                    break;
                case ThemeManager.Themes.HowlsMovingCastle:
                    themeComboBox.SelectedIndex = 11;
                    theme = 11;
                    break;
                case ThemeManager.Themes.luckybubblegum:
                    themeComboBox.SelectedIndex = 12;
                    theme = 12;
                    break;
                case ThemeManager.Themes.pluckoff:
                    themeComboBox.SelectedIndex = 13;
                    theme = 13;
                    break;
                case ThemeManager.Themes.frenchkiss:
                    themeComboBox.SelectedIndex = 14;
                    theme = 14;
                    break;
                case ThemeManager.Themes.ADreaminColor:
                    themeComboBox.SelectedIndex = 15;
                    theme = 15;
                    break;
                case ThemeManager.Themes.WindowsPhoneDark:
                    themeComboBox.SelectedIndex = 16;
                    theme = 16;
                    break;
            }
            if (Settings.GetSetting("NSFW") as string == "false")
                nsfwToggleSwitch.IsOn = false;
            else
                nsfwToggleSwitch.IsOn = true;
            listingLimitSlider.Value = Convert.ToDouble(Settings.GetSetting("ListingLimit"));

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
            #if WINDOWS_PHONE_APP
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
#endif
        }

        #if WINDOWS_PHONE_APP
        async void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            bool needsToRestart = false;

            if (themeComboBox.SelectedIndex != theme)
            {
                theme = themeComboBox.SelectedIndex;

                Settings.SetSetting("Theme", themeComboBox.SelectedIndex);
            }

            if (nsfwToggleSwitch.IsOn != Helpers.General.StringToBool((string)Settings.GetSetting("NSFW")))
                Settings.SetSetting("NSFW", nsfwToggleSwitch.IsOn.ToString());

            if (listingLimitSlider.Value != (double)Settings.GetSetting("ListingLimit"))
                Settings.SetSetting("ListingLimit", listingLimitSlider.Value);

            if (needsToRestart)
            {
                var dialog = new MessageDialog("You will need to restart the app for changes to take effect.", "Warning");
                await dialog.ShowAsync();
            }
        }
#endif

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            pageData.Theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")));
            var hexToColourConverter = new HexToColorConverter();
            this.Background = (SolidColorBrush)hexToColourConverter.Convert(pageData.Theme.Background, typeof(SolidColorBrush), null, null);
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void themeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)themeComboBox.SelectedIndex));

                var hexToColourConverter = new HexToColorConverter();

                themeAccentRectangle.Fill = (SolidColorBrush)hexToColourConverter.Convert(theme.Accent, typeof(SolidColorBrush), null, null);
                themeBackgroundRectangle.Fill = (SolidColorBrush)hexToColourConverter.Convert(theme.Background, typeof(SolidColorBrush), null, null);
                themeChromeRectangle.Fill = (SolidColorBrush)hexToColourConverter.Convert(theme.Chrome, typeof(SolidColorBrush), null, null);
                themeDisabledRectangle.Fill = (SolidColorBrush)hexToColourConverter.Convert(theme.DisabledForeground, typeof(SolidColorBrush), null, null);
                themeForegroundRectangle.Fill = (SolidColorBrush)hexToColourConverter.Convert(theme.Foreground, typeof(SolidColorBrush), null, null);
            }
            catch { }
        }
    }
}
