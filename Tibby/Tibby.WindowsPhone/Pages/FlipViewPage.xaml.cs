﻿using Snoo.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.ComponentModel;
using Snoo.Reddit.DataTypes;
using System.Diagnostics;
using Snoo.UserControls;
using System.Collections.ObjectModel;
using Snoo.Reddit.DataTypes.Prefixes;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Snoo.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class FlipViewPage : Page
    {
        private NavigationHelper navigationHelper;
        private FlipViewPageData pageData = new FlipViewPageData();

        public FlipViewPage()
        {
            this.InitializeComponent();

            this.DataContext = pageData;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            pageData.Posts = null;

            pageData.Theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")));
            var hexToColourConverter = new HexToColorConverter();
            this.Background = (SolidColorBrush)hexToColourConverter.Convert(pageData.Theme.Background, typeof(SolidColorBrush), null, null);

            if (e.PageState != null)
            {
                pageData = e.PageState["pageData"] as FlipViewPageData;
            }
            else
            {
                var data = e.NavigationParameter as FlipViewPostData;

                pageData.Posts = data.posts;
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            if (e.PageState == null || !e.PageState.ContainsKey("pageData"))
                e.PageState.Add("pageData", pageData);
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void Scroller_Tapped(object sender, TappedRoutedEventArgs e)
        {

        }

        private void PostItem_MediaTapped(object sender, EventArgs e)
        {
            this.Frame.Navigate(typeof(PostPage), (sender as PostItem).Data);
        }

        bool isIncrementing = false;
        private async void ScrollViewer_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            var result = scrollView.ScrollableHeight - scrollView.VerticalOffset;

            if (!isIncrementing && result <= 2500)
            {
                isIncrementing = true;

                var lastPost = pageData.Posts[pageData.Posts.Count - 1];

                var newData = (await Reddit.Reddit.GetPosts(Database.GetValue("lastSubreddit") as string, Reddit.Reddit.SubredditListing.Hot, "", lastPost.Data.Name, "", pageData.Posts.Count)).Data.Children;

                foreach (var i in newData)
                    pageData.Posts.Add(i);

                isIncrementing = false;
            }
        }
    }
}
