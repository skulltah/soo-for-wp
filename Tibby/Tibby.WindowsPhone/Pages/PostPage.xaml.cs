﻿using Snoo.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Snoo.Reddit.DataTypes;
using System.ComponentModel;
using Windows.ApplicationModel.DataTransfer;
using SnooHelper;
using Windows.UI.Xaml.Media.Imaging;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.UI.Xaml.Markup;
using System.Diagnostics;
using System.Net.Http;
using Windows.Storage.Streams;
using Windows.Storage;
using Snoo.Reddit.DataTypes.Prefixes;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Snoo.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PostPage : Page
    {
        private NavigationHelper navigationHelper;
        private PostPageData pageData = new PostPageData();

        public PostPage()
        {
            this.InitializeComponent();

            this.DataContext = pageData;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();

            dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager,
                DataRequestedEventArgs>(this.ShareLinkHandler);

            loadingControl.Show();
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            pageData.Theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")));
            var hexToColourConverter = new HexToColorConverter();
            this.Background = (SolidColorBrush)hexToColourConverter.Convert(pageData.Theme.Background, typeof(SolidColorBrush), null, null);

            if (e.PageState != null)
            {
                pageData = e.PageState["pageData"] as PostPageData;
            }
            else
            {
                pageData = e.NavigationParameter as PostPageData;

                await LoadData();
            }

            upvote.IsChecked = false;
            downvote.IsChecked = false;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            if (e.PageState == null || !e.PageState.ContainsKey("pageData"))
                e.PageState.Add("pageData", pageData);
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async Task LoadData()
        {
            titleTextBlock.Text = pageData.Post.Title;
            scoreRun.Text = pageData.Post.Score.ToString();
            hoursAgoRun.Text = "";
            subredditRun.Text = pageData.Post.Subreddit;
            numCommentsRun.Text = pageData.Post.NumComments.ToString();
            urlRun.Text = pageData.Post.Url;

            if (pageData.Post.Likes != null && pageData.Post.Likes != "")
            {
                if (pageData.Post.Likes.ToLower() == "true")
                    upvote.IsChecked = true;
                if (pageData.Post.Likes.ToLower() == "false")
                    downvote.IsChecked = true;
            }

            Debug.WriteLine(pageData.Post.Likes);

            var type = LinkTypeChecker.Check(pageData.Post.Url);

            switch (type)
            {
                case LinkTypeChecker.LinkType.Picture:
                    ShowHide(LinkTypeChecker.LinkType.Picture);

                    var bmp = new BitmapImage() { CreateOptions = BitmapCreateOptions.None };
                    bmp.UriSource = new Uri(pageData.Post.Url, UriKind.Absolute);
                    image.Source = bmp;
                    break;
                case LinkTypeChecker.LinkType.GIF:
                    ShowHide(LinkTypeChecker.LinkType.GIF);

                    mediaElement.Source = new Uri(await GFYCat.GetMP4(pageData.Post.Url), UriKind.Absolute);
                    mediaElement.Play();

                    saveToPhoneButton.IsEnabled = false;

                    loadingControl.Hide();
                    break;
                case LinkTypeChecker.LinkType.Album:
                    ShowHide(LinkTypeChecker.LinkType.Album);

                    saveToPhoneButton.IsEnabled = false;

                    loadingControl.Hide();
                    break;
                case LinkTypeChecker.LinkType.GFYCat:
                    ShowHide(LinkTypeChecker.LinkType.GFYCat);

                    loadingControl.Hide();
                    break;
                case LinkTypeChecker.LinkType.Website:
                    ShowHide(LinkTypeChecker.LinkType.Website);

                    if (!pageData.Post.IsSelf)
                    {
                        webView.Navigate(new Uri(pageData.Post.Url, UriKind.Absolute));

                        saveToPhoneButton.IsEnabled = false;
                    }
                    else
                    {
                        selfTextScrollViewer.Visibility = Visibility.Visible;
                        webView.Visibility = Visibility.Collapsed;

                        selfTextBlock.Text = pageData.Post.Selftext;

                        saveToPhoneButton.IsEnabled = false;

                        loadingControl.Hide();
                    }
                    break;
                case LinkTypeChecker.LinkType.YouTube:
                    ShowHide(LinkTypeChecker.LinkType.YouTube);

                    youtubeWebView.Navigate(new Uri(pageData.Post.Url, UriKind.Absolute));

                    saveToPhoneButton.IsEnabled = false;

                    loadingControl.Hide();
                    break;
            }
        }

        private void ShowHide(LinkTypeChecker.LinkType linkType)
        {
            switch (linkType)
            {
                case LinkTypeChecker.LinkType.Picture:
                    imageScrollViewer.Visibility = Visibility.Visible;
                    youtubeGrid.Visibility = Visibility.Collapsed;
                    mediaElement.Visibility = Visibility.Collapsed;
                    albumItemsControl.Visibility = Visibility.Collapsed;
                    webView.Visibility = Visibility.Collapsed;
                    selfTextScrollViewer.Visibility = Visibility.Collapsed;
                    break;
                case LinkTypeChecker.LinkType.GIF:
                    mediaElement.Visibility = Visibility.Visible;
                    imageScrollViewer.Visibility = Visibility.Collapsed;
                    youtubeGrid.Visibility = Visibility.Collapsed;
                    albumItemsControl.Visibility = Visibility.Collapsed;
                    webView.Visibility = Visibility.Collapsed;
                    selfTextScrollViewer.Visibility = Visibility.Collapsed;
                    break;
                case LinkTypeChecker.LinkType.Album:
                    albumItemsControl.Visibility = Visibility.Visible;
                    imageScrollViewer.Visibility = Visibility.Collapsed;
                    mediaElement.Visibility = Visibility.Collapsed;
                    youtubeGrid.Visibility = Visibility.Collapsed;
                    webView.Visibility = Visibility.Collapsed;
                    selfTextScrollViewer.Visibility = Visibility.Collapsed;
                    break;
                case LinkTypeChecker.LinkType.GFYCat:
                    break;
                case LinkTypeChecker.LinkType.Website:
                    webView.Visibility = Visibility.Visible;
                    imageScrollViewer.Visibility = Visibility.Collapsed;
                    mediaElement.Visibility = Visibility.Collapsed;
                    albumItemsControl.Visibility = Visibility.Collapsed;
                    youtubeGrid.Visibility = Visibility.Collapsed;
                    selfTextScrollViewer.Visibility = Visibility.Collapsed;
                    break;
                case LinkTypeChecker.LinkType.YouTube:
                    youtubeGrid.Visibility = Visibility.Visible;
                    imageScrollViewer.Visibility = Visibility.Collapsed;
                    mediaElement.Visibility = Visibility.Collapsed;
                    albumItemsControl.Visibility = Visibility.Collapsed;
                    webView.Visibility = Visibility.Collapsed;
                    selfTextScrollViewer.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        private void mediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            (sender as MediaElement).Play();
        }

        private void Image_ImageOpened(object sender, RoutedEventArgs e)
        {
            image.Width = Window.Current.Bounds.Width;
            loadingControl.Hide();
        }

        private void webView_ContentLoading(WebView sender, WebViewContentLoadingEventArgs args)
        {
            loadingControl.Hide();
        }

        bool isCollapsed = true;
        double actualHeight = 1;

        private void MediaRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
#if WINDOWS_PHONE_APP
            try
            {
                isCollapsed = !isCollapsed;

                if (!isCollapsed && actualHeight == 1)
                    actualHeight = TitleRoot.ActualHeight;

                if (isCollapsed)
                {
                    TitleRoot.Height = actualHeight;
                    commandBar.ClosedDisplayMode = AppBarClosedDisplayMode.Compact;
                }
                else
                {
                    TitleRoot.Height = 0;
                    commandBar.ClosedDisplayMode = AppBarClosedDisplayMode.Minimal;
                }
            }
            catch { }
#endif
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await Windows.System.Launcher.LaunchUriAsync(new System.Uri("metrotube:VideoPage?VideoID=" + YoutubeHelper.GetIDFromURL(pageData.Post.Url)));
        }

        async private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            await Windows.System.Launcher.LaunchUriAsync(new System.Uri("mytube:link=" + YoutubeHelper.GetIDFromURL(pageData.Post.Url)));
        }

        private void TitleRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            string param = pageData.Post.ID;

            this.Frame.Navigate(typeof(CommentsPage), param);
        }

        private async void downvote_Checked(object sender, RoutedEventArgs e)
        {
            if (upvote.IsChecked.Value)
                upvote.IsChecked = false;

            int vote = -1;

            await Reddit.Reddit.Vote(vote, pageData.Post.Name);
        }

        private async void downvote_Unchecked(object sender, RoutedEventArgs e)
        {
            int vote = 0;

            await Reddit.Reddit.Vote(vote, pageData.Post.Name);
        }

        private async void upvote_Checked(object sender, RoutedEventArgs e)
        {
            if (downvote.IsChecked.Value)
                downvote.IsChecked = false;

            int vote = 1;

            await Reddit.Reddit.Vote(vote, pageData.Post.Name);
        }

        private async void upvote_Unchecked(object sender, RoutedEventArgs e)
        {
            int vote = 0;

            await Reddit.Reddit.Vote(vote, pageData.Post.Name);
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            DataTransferManager.ShowShareUI();
        }

        private void ShareLinkHandler(DataTransferManager sender, DataRequestedEventArgs e)
        {
            DataRequest request = e.Request;
            request.Data.Properties.Title = pageData.Post.Title;
            request.Data.Properties.Description = "Shared from Snoo.";
            request.Data.SetWebLink(new Uri(pageData.Post.Url));
        }

        private async void saveToPhoneButton_Click(object sender, RoutedEventArgs e)
        {
            loadingControl.Show();

            Uri uri = new Uri(pageData.Post.Url);
            var fileName = Guid.NewGuid().ToString() + ".jpg";

            // download pic
            var bitmapImage = new BitmapImage();
            var httpClient = new HttpClient();
            var httpResponse = await httpClient.GetAsync(uri);
            byte[] b = await httpResponse.Content.ReadAsByteArrayAsync();

            // create a new in memory stream and datawriter
            using (var stream = new InMemoryRandomAccessStream())
            {
                using (DataWriter dw = new DataWriter(stream))
                {
                    // write the raw bytes and store
                    dw.WriteBytes(b);
                    await dw.StoreAsync();

                    // set the image source
                    stream.Seek(0);
                    bitmapImage.SetSource(stream);

                    // write to pictures library
                    var storageFile = await KnownFolders.PicturesLibrary.CreateFileAsync(
                        fileName,
                        CreationCollisionOption.ReplaceExisting);

                    using (var storageStream = await storageFile.OpenAsync(FileAccessMode.ReadWrite))
                    {
                        await RandomAccessStream.CopyAndCloseAsync(stream.GetInputStreamAt(0), storageStream.GetOutputStreamAt(0));
                    }
                }
            }

            loadingControl.Hide();
        }

        async private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            var uri = new Uri(pageData.Post.Url);

            await Windows.System.Launcher.LaunchUriAsync(uri);
        }

        private async void AppBarButton_Click_2(object sender, RoutedEventArgs e)
        {
            await Reddit.Reddit.Save(pageData.Post.Name);
        }
    }
}
