﻿using Snoo.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.ComponentModel;
using Snoo.Reddit.DataTypes.Prefixes;
using Snoo.Reddit.DataTypes;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Snoo.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ProfilePage : Page
    {
        private NavigationHelper navigationHelper;
        private ProfilePageData pageData = new ProfilePageData();

        string _username = "";

        public ProfilePage()
        {
            this.InitializeComponent();

            this.DataContext = pageData;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            pageData.Theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")));
            var hexToColourConverter = new HexToColorConverter();
            this.Background = (SolidColorBrush)hexToColourConverter.Convert(pageData.Theme.Background, typeof(SolidColorBrush), null, null);

            if (e.PageState != null)
                pageData = e.PageState["pageData"] as ProfilePageData;
            else
            {
                if (e.NavigationParameter != null)
                    await LoadData(e.NavigationParameter as string);
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            if (e.PageState == null || !e.PageState.ContainsKey("pageData"))
                e.PageState.Add("pageData", pageData);
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async Task LoadData(string username)
        {
            pageData.Profile = (await Reddit.Reddit.GetProfile(username)).Data;

            if(pageData.Profile.IsFriend)
            {
                addFreindButton.Visibility = Visibility.Collapsed;
                removeFreindButton.Visibility = Visibility.Visible;
            }
            else
            {
                addFreindButton.Visibility = Visibility.Visible;
                removeFreindButton.Visibility = Visibility.Collapsed;
            }

            var birthday = Helpers.General.FromUnixTime(pageData.Profile.Created);
            redditorForTextBlock.Text = birthday.ToString("ddd, dd MMM yyyy");

            DateTime today = DateTime.Today;
            DateTime next = birthday.AddYears(today.Year - birthday.Year);

            if (next < today)
                next = next.AddYears(1);

            int numDays = (next - today).Days;

            cakedayTextBlock.Text = numDays + " days";

            pageData.Submissions = (await Reddit.Reddit.GetUserSubmissions(username)).Data.Children;
            pageData.Comments = (await Reddit.Reddit.GetUserComments(username)).Data.Children;
        }

        private void pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private async void addFreindButton_Click(object sender, RoutedEventArgs e)
        {
            if (await Reddit.Reddit.Friend(_username))
            {
                addFreindButton.Visibility = Visibility.Collapsed;
                removeFreindButton.Visibility = Visibility.Visible;
            }
            else
            {
                addFreindButton.Visibility = Visibility.Visible;
                removeFreindButton.Visibility = Visibility.Collapsed;
            }
        }

        private async void removeFreindButton_Click(object sender, RoutedEventArgs e)
        {
            if (await Reddit.Reddit.Unriend(_username))
            {
                addFreindButton.Visibility = Visibility.Visible;
                removeFreindButton.Visibility = Visibility.Collapsed;
            }
            else
            {
                addFreindButton.Visibility = Visibility.Collapsed;
                removeFreindButton.Visibility = Visibility.Visible;
            }
        }

        private t3_ lastClickedPost { get; set; }
        private async void ListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                await Task.Delay(100);

                var post = lastClickedPost;

                var data = new PostPageData()
                {
                    Post = lastClickedPost
                };

                this.Frame.Navigate(typeof(PostPage), data);
            }
            catch { }
        }

        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            lastClickedPost = (sender as Grid).DataContext as t3_;
        }
    }
}
