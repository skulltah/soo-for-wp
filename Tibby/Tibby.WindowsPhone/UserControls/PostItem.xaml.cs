﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Tibby.Pages;
using Tibby.Reddit.DataTypes;
using Tibby.Reddit.DataTypes.Prefixes;
using TibbyHelper;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Tibby.UserControls
{
    public sealed partial class PostItem : UserControl
    {
        public static readonly DependencyProperty DataProperty =
       DependencyProperty.Register(
           "Data",
           typeof(FlipViewSingleDataItem),
           typeof(PostItem),
           new PropertyMetadata(null));

        public FlipViewSingleDataItem Data
        {
            get { return (FlipViewSingleDataItem)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        public event EventHandler MediaTapped;

        public PostItem()
        {
            this.InitializeComponent();

            var theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")));
            var hexToColourConverter = new HexToColorConverter();
            titleGrid.Background = (SolidColorBrush)hexToColourConverter.Convert(theme.Chrome, typeof(SolidColorBrush), null, null);
            rectangle.Fill = (SolidColorBrush)hexToColourConverter.Convert(theme.Chrome, typeof(SolidColorBrush), null, null);
            titleTextBlock.Foreground = (SolidColorBrush)hexToColourConverter.Convert(theme.Foreground, typeof(SolidColorBrush), null, null);
            tapToViewHeaderTextBlock.Foreground = (SolidColorBrush)hexToColourConverter.Convert(theme.Foreground, typeof(SolidColorBrush), null, null);
            tapToViewTextBlock.Foreground = (SolidColorBrush)hexToColourConverter.Convert(theme.Foreground, typeof(SolidColorBrush), null, null);

            Loaded += PostItem_Loaded;
        }

        void PostItem_Loaded(object sender, RoutedEventArgs e)
        {
            var type = LinkTypeChecker.Check(Data.URL);

            switch (type)
            {
                case LinkTypeChecker.LinkType.Picture:
                    tapToViewHeaderTextBlock.Visibility = Visibility.Collapsed;
                    tapToViewTextBlock.Visibility = Visibility.Collapsed;
                    break;
                case LinkTypeChecker.LinkType.GIF:
                    try
                    {
                        image.Source = new BitmapImage(new Uri(Data.Thumbnail, UriKind.Absolute));
                    }
                    catch { }
                    tapToViewHeaderTextBlock.Text = "GIF";
                    tapToViewTextBlock.Text = "tap to play";
                    image.Opacity = 0.6;
                    break;
                case LinkTypeChecker.LinkType.Album:
                    tapToViewHeaderTextBlock.Text = "album";
                    tapToViewTextBlock.Text = "tap to view";
                    break;
                case LinkTypeChecker.LinkType.GFYCat:
                    tapToViewHeaderTextBlock.Text = "GFYCat";
                    tapToViewTextBlock.Text = "tap to play";
                    break;
                case LinkTypeChecker.LinkType.Website:
                    tapToViewHeaderTextBlock.Text = "website";
                    tapToViewTextBlock.Text = "tap to view";
                    break;
                case LinkTypeChecker.LinkType.YouTube:
                    tapToViewHeaderTextBlock.Text = "YouTube";
                    tapToViewTextBlock.Text = "tap to view";
                    break;
            }

            LayoutRoot.DataContext = Data;
        }

        bool didUserTap = false;

        private async void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (!didUserTap)
            {
                var type = LinkTypeChecker.Check(Data.URL);

                bool needsToTriggerMediaTapped = false;

                switch (type)
                {
                    case LinkTypeChecker.LinkType.Picture:
                        needsToTriggerMediaTapped = true;
                        break;
                    case LinkTypeChecker.LinkType.GIF:
                        tapToViewHeaderTextBlock.Visibility = Visibility.Collapsed;
                        tapToViewTextBlock.Visibility = Visibility.Collapsed;
                        mediaElement.Visibility = Visibility.Visible;
                        mediaElement.Source = new Uri(await GFYCat.GetMP4(Data.URL), UriKind.Absolute);
                        mediaElement.Play();
                        break;
                    case LinkTypeChecker.LinkType.Album:
                        needsToTriggerMediaTapped = true;
                        break;
                    case LinkTypeChecker.LinkType.GFYCat:
                        needsToTriggerMediaTapped = true;
                        break;
                    case LinkTypeChecker.LinkType.Website:
                        if (Data.IsSelf)
                            ((Frame)Window.Current.Content).Navigate(typeof(CommentsPage), Data.Name.Remove(0, 3));
                        else
                            needsToTriggerMediaTapped = true;
                        break;
                    case LinkTypeChecker.LinkType.YouTube:
                        needsToTriggerMediaTapped = true;
                        break;
                }

                didUserTap = true;

                if (needsToTriggerMediaTapped)
                {
                    var postPageData = new PostPageData()
                    {
                        Theme = null,
                        Post = this.Data,
                        Comments = null
                    };

                    ((Frame)Window.Current.Content).Navigate(typeof(PostPage), postPageData);
                }
            }
        }

        private void mediaElement_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var Sender = sender as MediaElement;

            switch (Sender.PlaybackRate.ToString())
            {
                case "1":
                    Sender.PlaybackRate = 0.5;
                    return;
                case "0.5":
                    Sender.PlaybackRate = 0;
                    return;
                case "0":
                    Sender.PlaybackRate = 1;
                    return;
            }
        }

        private void mediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            (sender as MediaElement).Play();
        }

        private void titleGrid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ((Frame)Window.Current.Content).Navigate(typeof(CommentsPage), Data.Name.Remove(0, 3));
        }
    }
}
