﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Tibby.UserControls
{
    public sealed partial class LoadingControl : UserControl
    {
        private BasicPageData pageData = new BasicPageData();

        public LoadingControl()
        {
            this.InitializeComponent();

            try
            {
                pageData.Theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")));
            }
            catch
            {
                Settings.LoadSettings();
                ThemeManager.LoadThemes();
                pageData.Theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")));
            }

            var hexToColourConverter = new HexToColorConverter();
            this.DataContext = pageData;
        }

        public void Show()
        {
            this.progressRing.IsActive = true;
            this.Visibility = Visibility.Visible;
        }

        public void Hide()
        {
            this.progressRing.IsActive = false;
            this.Visibility = Visibility.Collapsed;
        }
    }
}
