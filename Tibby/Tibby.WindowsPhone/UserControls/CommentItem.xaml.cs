﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Tibby.Pages;
using Tibby.Reddit.DataTypes.Prefixes;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Tibby.UserControls
{
    public sealed partial class CommentItem : UserControl
    {
        t1_ t1 { get; set; }

        public CommentItem()
        {
            this.InitializeComponent();
        }

        public async Task LoadComment(t1_ data)
        {
            foreach (var r in data.Replies.Data.Children)
            {
                try
                {
                    LayoutRoot.DataContext = data;
                    t1 = data;

                    string date = "";
                    var dateTime = DateTime.Now.Subtract(Helpers.General.FromUnixTime(data.Created));

                    save.IsChecked = data.Saved;

                    if (data.Likes != null && data.Likes != "")
                    {
                        if (data.Likes.ToLower() == "true")
                            upvote.IsChecked = true;
                        if (data.Likes.ToLower() == "false")
                            downvote.IsChecked = true;
                    }

                    if (dateTime.Days > 0)
                        date = dateTime.Days.ToString() + " days, ";
                    if (dateTime.Hours > 0)
                    {
                        date = date + dateTime.Hours.ToString() + " hours and ";
                        date = date + dateTime.Minutes.ToString() + " minutes ago";
                    }
                    else
                    {
                        date = date + dateTime.Minutes.ToString() + " minutes ago";
                    }

                    dateRun.Text = date;

                    var cc = new CommentItem();
                    await cc.LoadComment(r.Data);
                    cc.Margin = new Thickness(0, 5, 0, 5);

                    itemsControl.Items.Add(cc);
                }
                catch { }
            }
        }

        private void commentTextBlock_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            switch (itemsControl.Visibility)
            {
                case Windows.UI.Xaml.Visibility.Visible:
                    itemsControl.Visibility = Visibility.Collapsed;
                    commentTextBlock.TextTrimming = TextTrimming.WordEllipsis;
                    commentTextBlock.MaxLines = 1;
                    actionsGrid.Visibility = Visibility.Collapsed;
                    break;
                case Windows.UI.Xaml.Visibility.Collapsed:
                    itemsControl.Visibility = Visibility.Visible;
                    commentTextBlock.TextTrimming = TextTrimming.None;
                    commentTextBlock.MaxLines = 0;
                    actionsGrid.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        private void commentTextBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            switch (actionsGrid.Visibility)
            {
                case Windows.UI.Xaml.Visibility.Visible:
                    actionsGrid.Visibility = Visibility.Collapsed;
                    break;
                case Windows.UI.Xaml.Visibility.Collapsed:
                    actionsGrid.Visibility = Visibility.Visible;
                    break;
            }
        }

        private async void AppBarToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            if (!(await Reddit.Reddit.Save(t1.Name)))
                (sender as AppBarToggleButton).IsChecked = false;
        }

        private async void AppBarToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!(await Reddit.Reddit.Unsave(t1.Name)))
                (sender as AppBarToggleButton).IsChecked = true;
        }

        private async void AppBarToggleButton_Checked_1(object sender, RoutedEventArgs e)
        {
            if (upvote.IsChecked.Value)
                upvote.IsChecked = false;

            int vote = -1;

            await Reddit.Reddit.Vote(vote, t1.Name);
        }

        private async void AppBarToggleButton_Unchecked_1(object sender, RoutedEventArgs e)
        {
            int vote = 0;

            await Reddit.Reddit.Vote(vote, t1.Name);
        }

        private async void AppBarToggleButton_Checked_2(object sender, RoutedEventArgs e)
        {
            if (downvote.IsChecked.Value)
                downvote.IsChecked = false;

            int vote = 1;

            await Reddit.Reddit.Vote(vote, t1.Name);
        }

        private async void AppBarToggleButton_Unchecked_2(object sender, RoutedEventArgs e)
        {
            int vote = 0;

            await Reddit.Reddit.Vote(vote, t1.Name);
        }

        private async void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            Settings.SetSetting("ThingToCommentTo", t1.Name);

            var dialog = new Dialogs.CommentDialog();
            await dialog.ShowAsync();
        }

        private void TextBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ((Frame)Window.Current.Content).Navigate(typeof(ProfilePage), t1.Author);
        }
    }
}
