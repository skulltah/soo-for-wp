﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Snoo.Pages;
using Snoo.Reddit.DataTypes.Prefixes;
using SnooHelper;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Snoo.UserControls
{
    public sealed partial class CommentItem : UserControl
    {
        t1_ t1 { get; set; }

        public CommentItem()
        {
            this.InitializeComponent();
        }

        public async Task LoadComment(t1_ data)
        {
            LayoutRoot.DataContext = data;
            t1 = data;

            commentTextBlock.Text = WebUtility.HtmlDecode(data.Body);

            string date = "";
            var dateTime = Helpers.General.FromUnixTime(data.Created).Subtract(DateTime.Now);

            save.IsChecked = data.Saved;

            if (data.Likes != null && data.Likes != "")
            {
                if (data.Likes.ToLower() == "true")
                    upvote.IsChecked = true;
                if (data.Likes.ToLower() == "false")
                    downvote.IsChecked = true;
            }

            if (dateTime.Days > 0)
                date = dateTime.Days.ToString() + " days, ";
            if (dateTime.Hours > 0)
            {
                date = date + dateTime.Hours.ToString() + " hours and ";
                date = date + dateTime.Minutes.ToString() + " minutes ago";
            }
            else
            {
                date = date + dateTime.Minutes.ToString() + " minutes ago";
            }

            dateRun.Text = date.Replace("-", "");

            foreach (var link in Helpers.General.GetLinks(WebUtility.HtmlDecode(data.Body)))
            {
                var lType = LinkTypeChecker.Check(link);

                if (lType == LinkTypeChecker.LinkType.Picture)
                {
                    var img = new Image()
                    {
                        Source = new BitmapImage(new Uri(link, UriKind.Absolute)),
                        Stretch = Stretch.Uniform,
                        HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch,
                        Margin = new Thickness(5),
                        Tag = link
                    };

                    img.Tapped += img_Tapped;

                    linksPanel.Children.Add(img);
                }
                else
                {
                    var hLink = new HyperlinkButton()
                    {
                        Content = link,
                        Foreground = Resources["PhoneAccentBrush"] as Brush,
                        FontSize = 24,
                        Margin = new Thickness(5)
                    };

                    hLink.Click += hLink_Click;

                    linksPanel.Children.Add(hLink);
                }
            }

            foreach (var r in data.Replies.Data.Children)
            {
                try
                {
                    var cc = new CommentItem();
                    cc.Margin = new Thickness(0, 1, 0, 1);

                    await cc.LoadComment(r.Data);

                    itemsControl.Items.Add(cc);
                }
                catch (NullReferenceException) { }
                catch { }
            }
        }

        async void img_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var url = new Uri((sender as Image).Tag.ToString(), UriKind.Absolute);
            await Windows.System.Launcher.LaunchUriAsync(url);
        }

        async void hLink_Click(object sender, RoutedEventArgs e)
        {
            var url = new Uri((sender as HyperlinkButton).Content.ToString(), UriKind.Absolute);
            await Windows.System.Launcher.LaunchUriAsync(url);
        }

        private void commentTextBlock_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            switch (itemsControl.Visibility)
            {
                case Windows.UI.Xaml.Visibility.Visible:
                    itemsControl.Visibility = Visibility.Collapsed;
                    commentTextBlock.TextTrimming = TextTrimming.WordEllipsis;
                    commentTextBlock.MaxLines = 1;
                    actionsGrid.Visibility = Visibility.Collapsed;
                    break;
                case Windows.UI.Xaml.Visibility.Collapsed:
                    itemsControl.Visibility = Visibility.Visible;
                    commentTextBlock.TextTrimming = TextTrimming.None;
                    commentTextBlock.MaxLines = 0;
                    actionsGrid.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        private void commentTextBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            switch (actionsGrid.Visibility)
            {
                case Windows.UI.Xaml.Visibility.Visible:
                    actionsGrid.Visibility = Visibility.Collapsed;
                    break;
                case Windows.UI.Xaml.Visibility.Collapsed:
                    actionsGrid.Visibility = Visibility.Visible;
                    break;
            }
        }

        private async void AppBarToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            if (!(await Reddit.Reddit.Save(t1.Name)))
                (sender as AppBarToggleButton).IsChecked = false;
        }

        private async void AppBarToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!(await Reddit.Reddit.Unsave(t1.Name)))
                (sender as AppBarToggleButton).IsChecked = true;
        }

        private async void AppBarToggleButton_Checked_1(object sender, RoutedEventArgs e)
        {
            if (upvote.IsChecked.Value)
                upvote.IsChecked = false;

            int vote = -1;

            await Reddit.Reddit.Vote(vote, t1.Name);
        }

        private async void AppBarToggleButton_Unchecked_1(object sender, RoutedEventArgs e)
        {
            int vote = 0;

            await Reddit.Reddit.Vote(vote, t1.Name);
        }

        private async void AppBarToggleButton_Checked_2(object sender, RoutedEventArgs e)
        {
            if (downvote.IsChecked.Value)
                downvote.IsChecked = false;

            int vote = 1;

            await Reddit.Reddit.Vote(vote, t1.Name);
        }

        private async void AppBarToggleButton_Unchecked_2(object sender, RoutedEventArgs e)
        {
            int vote = 0;

            await Reddit.Reddit.Vote(vote, t1.Name);
        }

        private async void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
#if WINDOWS_PHONE_APP
            Settings.SetSetting("ThingToCommentTo", t1.Name);

            var dialog = new CommentDialog();
            await dialog.ShowAsync();
#endif
        }

        private void TextBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ((Frame)Window.Current.Content).Navigate(typeof(ProfilePage), t1.Author);
        }
    }
}
