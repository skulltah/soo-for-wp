﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Snoo.Reddit;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Pickers.Provider;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Snoo
{
    public sealed partial class LoginDialog : ContentDialog
    {
        public LoginDialog()
        {
            this.InitializeComponent();
        }

        private async void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var dialog = new MessageDialog("Please fill in both your username and your password.", "Error");

            if (username.Text == "" || password.Password == "")
                await dialog.ShowAsync();

            await LoginHelper.Login(username.Text, password.Password);
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            //string uriToLaunch = @"http://i.reddit.com/register.compact?keep_extension=true";
            //var uri = new Uri(uriToLaunch);
            //await Windows.System.Launcher.LaunchUriAsync(uri);

            Settings.SetSetting("NeedsToRegister", "true");
            this.Hide();
        }
    }
}
