﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Pickers.Provider;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Snoo
{
    public sealed partial class RegisterDialog : ContentDialog
    {
        string iden = "";

        public RegisterDialog()
        {
            this.InitializeComponent();

            LoadData();
        }

        private async void LoadData()
        {
            var cIden = Settings.GetSetting("CaptchaIden") as string;

            if (cIden != null && cIden != "")
            {
                iden = cIden;
                Settings.SetSetting("CaptchaIden", "");
            }
            else
            {
                var captchaRequest = await Reddit.Reddit.GetNewCaptcha();
                iden = captchaRequest.Json.Data.Iden;
            }

            var bitmap = await Reddit.Reddit.GetCaptcha(iden);

            captchaImage.Source = bitmap;
        }

        private async void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var dialog = new MessageDialog("Please fill all of the text boxes.", "Error");

            if (username.Text == "" || password.Password == "")
                await dialog.ShowAsync();
            else
            {
                var result = await Reddit.Reddit.Register(captchaBox.Text, "", iden, password.Password, username.Text);

                if (result.Json.Errors.Length > 0)
                {
                    Settings.SetSetting("CaptchaIden", result.Json.Captcha);
                    Settings.SetSetting("RegisterErrorTitle", result.Json.Errors[0][0]);
                    Settings.SetSetting("RegisterError", result.Json.Errors[0][1]);
                }
                else
                {
                    Database.SetValue("cookie", result.Json.Data.Cookie);
                    Database.SetValue("modhash", result.Json.Data.Modhash);
                    Database.SetValue("username", username.Text);
                    Database.SetValue("IsLoggedIn", "true");
                }
            }
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }
    }
}
