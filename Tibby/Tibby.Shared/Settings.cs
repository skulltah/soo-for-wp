﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Storage;

namespace Snoo
{
    public class Settings
    {
        public static void LoadSettings()
        {
            if (!ApplicationData.Current.LocalSettings.Containers.ContainsKey("Settings"))
                ApplicationData.Current.LocalSettings.CreateContainer("Settings", ApplicationDataCreateDisposition.Always);

            if (!ApplicationData.Current.LocalSettings.Containers["Settings"].Values.ContainsKey("emptyKey"))
            {
                SetSetting("emptyKey", 0);
                SetSetting("Theme", 0);
                SetSetting("NSFW", "false");
                SetSetting("ListingLimit", (double)25);
            }
        }

        public static void SetSetting(string property, object value)
        {
            if (!ApplicationData.Current.LocalSettings.Containers.ContainsKey("Settings"))
                ApplicationData.Current.LocalSettings.CreateContainer("Settings", ApplicationDataCreateDisposition.Always);

            var container = ApplicationData.Current.LocalSettings.Containers["Settings"].Values;

            if (container.ContainsKey(property)) container[property] = value;
            else container.Add(property, value);
        }

        public static object GetSetting(string property)
        {
            if (!ApplicationData.Current.LocalSettings.Containers.ContainsKey("Settings"))
                ApplicationData.Current.LocalSettings.CreateContainer("Settings", ApplicationDataCreateDisposition.Always);

            var container = ApplicationData.Current.LocalSettings.Containers["Settings"].Values;

            if (container.ContainsKey(property))
                return container[property];
            else return null;
        }
    }
}
