﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Storage;

namespace Snoo
{
    public class Database
    {
        private static Windows.Foundation.Collections.IPropertySet settings = ApplicationData.Current.LocalSettings.Values;

        public static void SetValue(string name, object value)
        {
            if (settings.ContainsKey(name)) settings[name] = value;
            else settings.Add(name, value);
        }

        public static object GetValue(string name)
        {
            if (settings.ContainsKey(name)) return settings[name];
            else return null;
        }

        public static void RemoveValue(string name)
        {
            if (settings.ContainsKey(name)) settings.Remove(name);
        }

        public static void RemoveValues(string[] names)
        {
            foreach (var name in names)
                if (settings.ContainsKey(name)) settings.Remove(name);
        }

        public static void RemoveAllValues(string name)
        {
            settings.Clear();
        }
    }
    
    public class CompositeDatabase
    {
        private static ApplicationDataCompositeValue settings = new ApplicationDataCompositeValue(); 

        public static void SetValue(string name, object value)
        {
            if (settings.ContainsKey(name)) settings[name] = value;
            else settings.Add(name, value);
        }

        public static object GetValue(string name)
        {
            if (settings.ContainsKey(name)) return settings[name];
            else return null;
        }

        public static void RemoveValue(string name)
        {
            if (settings.ContainsKey(name)) settings.Remove(name);
        }

        public static void RemoveValues(string[] names)
        {
            foreach (var name in names)
                if (settings.ContainsKey(name)) settings.Remove(name);
        }

        public static void RemoveAllValues(string name)
        {
            settings.Clear();
        }
    }
}
