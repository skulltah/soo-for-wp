﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SnooHelper
{
    public class GFYCat
    {
        public static async Task<string> GetMP4(string url)
        {
            var client = new HttpClient();
            var data = await client.GetStringAsync(string.Format("http://upload.gfycat.com/transcode/{0}?fetchUrl={1}", new Random(123).Next(1000).ToString(), url));

            GFYCatResponse m = JsonConvert.DeserializeObject<GFYCatResponse>(data);

            var GFYName = m.Gfyname;

            var client2 = new HttpClient();
            var data2 = await client2.GetStringAsync(string.Format("http://gfycat.com/cajax/get/{0}", GFYName));

            GFYCatResponse2 r = JsonConvert.DeserializeObject<GFYCatResponse2>(data2);

            return r.GfyItem.Mp4Url;
        }

        public static async Task<string> GetWEBM(string url)
        {
            var client = new HttpClient();
            var data = await client.GetStringAsync(string.Format("http://upload.gfycat.com/transcode/{0}?fetchUrl={1}", new Random(123).Next(1000).ToString(), url));

            GFYCatResponse m = JsonConvert.DeserializeObject<GFYCatResponse>(data);

            var GFYName = m.Gfyname;

            var client2 = new HttpClient();
            var data2 = await client2.GetStringAsync(string.Format("http://gfycat.com/cajax/get/{0}", GFYName));

            GFYCatResponse2 r = JsonConvert.DeserializeObject<GFYCatResponse2>(data2);

            return r.GfyItem.WebmUrl;
        }
    }
}
