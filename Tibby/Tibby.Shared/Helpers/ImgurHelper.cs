﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SnooHelper
{
    public class ImgurHelper
    {
        public static async Task<List<string>> GetAlbumImages(string url)
        {
            string id = "";

            Regex imgurRegex = new Regex(@"http://(?:i\.imgur\.com/(?<id>.*?)\.(?:jpg|png|gif)|imgur\.com/(?:album/)?(?<id>.*)$|imgur\.com/(?:a/)?(?<id>.*))$");
            Match imgurMatch = imgurRegex.Match(url);
            if (imgurMatch.Success)
                id = imgurMatch.Groups["id"].Value;

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Accept", "application/Json");
            client.DefaultRequestHeaders.Add("Authorization", "Client-ID 988f7020163f8c2");

            var response = await client.GetAsync(new Uri(string.Format("https://api.imgur.com/3/album/{0}/images", id)));
            var data = response.Content.ReadAsStringAsync().Result;

            client.Dispose();

            ImgurAlbumImages m = JsonConvert.DeserializeObject<ImgurAlbumImages>(data);

            List<string> urls = new List<string>();

            foreach (var i in m.Data)
                urls.Add(i.Link);

            return urls;
        }
    }
}
