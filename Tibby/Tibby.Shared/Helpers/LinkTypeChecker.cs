﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SnooHelper
{
    public class LinkTypeChecker
    {
        public static LinkType Check(string url)
        {
            if (url.EndsWith(".gif")) return LinkType.GIF;
            if (Regex.IsMatch(url, @"http://(?:i\.imgur\.com/(?<id>.*?)\.(?:jpg|png|gif))")) return LinkType.Picture;
            if (url.EndsWith(".png") || url.EndsWith(".jpg") || url.EndsWith(".jpeg")) return LinkType.Picture;
            if (url.Contains("imgur.") && !url.Contains("/a/") && !url.Contains("/album/")) return LinkType.Picture;
            if (Regex.IsMatch(url, @"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)")) return LinkType.YouTube;
            if (url.Contains("gfycat.")) return LinkType.GFYCat;
            if (Regex.IsMatch(url, @"http://(?:i\.imgur\.com/(?:album/)?(?<id>.*)$|imgur\.com/(?:a/)?(?<id>.*))$")) return LinkType.Album;

            return LinkType.Website;
        }

        public enum LinkType
        {
            Picture,
            YouTube,
            Website,
            GIF,
            Album,
            GFYCat
        }
    }
}
