﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SnooHelper
{
    public class YoutubeHelper
    {
        public static string GetIDFromURL(string url)
        {
            Regex YoutubeVideoRegex = new Regex(@"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)");
            Match youtubeMatch = YoutubeVideoRegex.Match(url);

            string id = string.Empty;

            if (youtubeMatch.Success)
                return youtubeMatch.Groups[1].Value;

            return null;
        }
    }
}
