﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snoo
{
    public class MyConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            if (objectType == typeof(int))
                return true;

            if (objectType == typeof(bool))
                return true;

            if (objectType == typeof(double))
                return true;

            return false;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (objectType == typeof(int))
            {
                if (reader.Value == null) return 0;
                return Convert.ToInt32(reader.Value);
            }

            if (objectType == typeof(bool))
            {
                try
                {
                    if (reader.Value == null) return false;
                    return Convert.ToBoolean(reader.Value);
                }
                catch { return false; }
            }

            if (objectType == typeof(double))
            {
                if (reader.Value == null) return 0;
                return Convert.ToDouble(reader.Value);
            }

            return reader.Value;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
