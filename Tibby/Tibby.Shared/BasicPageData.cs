﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Snoo.Reddit.DataTypes;
using System.Collections.ObjectModel;
using Snoo;

namespace Snoo
{
    public class BasicPageData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }


        Theme _theme;
        public Theme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                NotifyPropertyChanged("Theme");
            }
        }
    }

    public class FlipViewPostData
    {
        public ObservableCollection<Posts.Child> posts { get; set; }
        public int index { get; set; }
    }
}
