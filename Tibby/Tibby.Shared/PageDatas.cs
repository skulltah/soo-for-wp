﻿using Snoo.Reddit.DataTypes;
using Snoo.Reddit.DataTypes.Prefixes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace Snoo
{
    public class CommentsPageData : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }


        Theme _theme;
        public Theme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                NotifyPropertyChanged("Theme");
            }
        }

        string _postID;
        public string PostID
        {
            get
            {
                return _postID;
            }
            set
            {
                _postID = value;
                NotifyPropertyChanged("PostID");
            }
        }
    }

    public class FlipViewPageData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }


        Theme _theme;
        public Theme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                NotifyPropertyChanged("Theme");
            }
        }

        ObservableCollection<Posts.Child> _posts;
        public ObservableCollection<Posts.Child> Posts
        {
            get
            {
                return _posts;
            }
            set
            {
                _posts = value;
                NotifyPropertyChanged("Posts");
            }
        }
    }

    public class PostPageData : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }


        Theme _theme;
        public Theme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                NotifyPropertyChanged("Theme");
            }
        }

        t3_ _post;
        public t3_ Post
        {
            get
            {
                return _post;
            }
            set
            {
                _post = value;
                NotifyPropertyChanged("Post");
            }
        }

        Comments _comments;
        public Comments Comments
        {
            get
            {
                return _comments;
            }
            set
            {
                _comments = value;
                NotifyPropertyChanged("Comments");
            }
        }
    }

    public class ProfilePageData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }


        Theme _theme;
        public Theme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                NotifyPropertyChanged("Theme");
            }
        }

        t2_ _profile;
        public t2_ Profile
        {
            get
            {
                return _profile;
            }
            set
            {
                _profile = value;
                NotifyPropertyChanged("Profile");
            }
        }

        ObservableCollection<Posts.Child> _submissions;
        public ObservableCollection<Posts.Child> Submissions
        {
            get
            {
                return _submissions;
            }
            set
            {
                _submissions = value;
                NotifyPropertyChanged("Submissions");
            }
        }

        ObservableCollection<UserComments.Child> _comments;
        public ObservableCollection<UserComments.Child> Comments
        {
            get
            {
                return _comments;
            }
            set
            {
                _comments = value;
                NotifyPropertyChanged("Comments");
            }
        }
    }

    public class SearchPageData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }


        Theme _theme;
        public Theme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                NotifyPropertyChanged("Theme");
            }
        }

        ObservableCollection<Subreddits.Child> _subreddits;
        public ObservableCollection<Subreddits.Child> Subreddits
        {
            get
            {
                return _subreddits;
            }
            set
            {
                _subreddits = value;
                NotifyPropertyChanged("Subreddits");
            }
        }

        ObservableCollection<SearchResults.Child> _posts;
        public ObservableCollection<SearchResults.Child> Posts
        {
            get
            {
                return _posts;
            }
            set
            {
                _posts = value;
                NotifyPropertyChanged("Posts");
            }
        }
    }

    public class SubredditPageData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }


        Theme _theme;
        public Theme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                NotifyPropertyChanged("Theme");
            }
        }


        ObservableCollection<Posts.Child> _hot;
        public ObservableCollection<Posts.Child> Hot
        {
            get
            {
                return _hot;
            }
            set
            {
                _hot = value;
                NotifyPropertyChanged("Hot");
            }
        }


        ObservableCollection<Posts.Child> _new;
        public ObservableCollection<Posts.Child> New
        {
            get
            {
                return _new;
            }
            set
            {
                _new = value;
                NotifyPropertyChanged("New");
            }
        }


        ObservableCollection<Posts.Child> _top;
        public ObservableCollection<Posts.Child> Top
        {
            get
            {
                return _top;
            }
            set
            {
                _top = value;
                NotifyPropertyChanged("Top");
            }
        }


        ObservableCollection<Posts.Child> _controversial;
        public ObservableCollection<Posts.Child> Controversial
        {
            get
            {
                return _controversial;
            }
            set
            {
                _controversial = value;
                NotifyPropertyChanged("Controversial");
            }
        }
    }

    public class SubscriptionsPageData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }


        Theme _theme;
        public Theme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                NotifyPropertyChanged("Theme");
            }
        }

        Subreddits _subreddits;
        public Subreddits Subreddits
        {
            get
            {
                return _subreddits;
            }
            set
            {
                _subreddits = value;
                NotifyPropertyChanged("Subreddits");
            }
        }

        Subreddits _popularSubreddits;
        public Subreddits PopularSubreddits
        {
            get
            {
                return _popularSubreddits;
            }
            set
            {
                _popularSubreddits = value;
                NotifyPropertyChanged("PopularSubreddits");
            }
        }
    }

}
