﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Snoo
{
    public class ThemeManager
    {
        public enum Themes
        {
            Default,
            AdriftinDreams,
            Wordless,
            DreamMagnet,
            CoupDeGrace,
            NewlyRisenMoon,
            MachuPicchu,
            GiantGoldfish,
            ThoughtProvoking,
            Terra,
            mellonballsurprise,
            HowlsMovingCastle,
            luckybubblegum,
            pluckoff,
            frenchkiss,
            ADreaminColor,
            WindowsPhoneDark
        }

        public static int ThemeToInt(Themes theme)
        {
            switch (theme)
            {
                case ThemeManager.Themes.Default:
                    return 0;
                case ThemeManager.Themes.AdriftinDreams:
                    return 1;
                case ThemeManager.Themes.Wordless:
                    return 2;
                case ThemeManager.Themes.DreamMagnet:
                    return 3;
                case ThemeManager.Themes.CoupDeGrace:
                    return 4;
                case ThemeManager.Themes.NewlyRisenMoon:
                    return 5;
                case ThemeManager.Themes.MachuPicchu:
                    return 6;
                case ThemeManager.Themes.GiantGoldfish:
                    return 7;
                case ThemeManager.Themes.ThoughtProvoking:
                    return 8;
                case ThemeManager.Themes.Terra:
                    return 9;
                case ThemeManager.Themes.mellonballsurprise:
                    return 10;
                case ThemeManager.Themes.HowlsMovingCastle:
                    return 11;
                case ThemeManager.Themes.luckybubblegum:
                    return 12;
                case ThemeManager.Themes.pluckoff:
                    return 13;
                case ThemeManager.Themes.frenchkiss:
                    return 14;
                case ThemeManager.Themes.ADreaminColor:
                    return 15;
                case ThemeManager.Themes.WindowsPhoneDark:
                    return 16;
            }

            return 0;
        }

        public static Themes IntToTheme(int theme)
        {
            switch (theme)
            {
                case 0:
                    return ThemeManager.Themes.Default;
                case 1:
                    return ThemeManager.Themes.AdriftinDreams;
                case 2:
                    return ThemeManager.Themes.Wordless;
                case 3:
                    return ThemeManager.Themes.DreamMagnet;
                case 4:
                    return ThemeManager.Themes.CoupDeGrace;
                case 5:
                    return ThemeManager.Themes.NewlyRisenMoon;
                case 6:
                    return ThemeManager.Themes.MachuPicchu;
                case 7:
                    return ThemeManager.Themes.GiantGoldfish;
                case 8:
                    return ThemeManager.Themes.ThoughtProvoking;
                case 9:
                    return ThemeManager.Themes.Terra;
                case 10:
                    return ThemeManager.Themes.mellonballsurprise;
                case 11:
                    return ThemeManager.Themes.HowlsMovingCastle;
                case 12:
                    return ThemeManager.Themes.luckybubblegum;
                case 13:
                    return ThemeManager.Themes.pluckoff;
                case 14:
                    return ThemeManager.Themes.frenchkiss;
                case 15:
                    return ThemeManager.Themes.ADreaminColor;
                case 16:
                    return ThemeManager.Themes.WindowsPhoneDark;
            }

            return ThemeManager.Themes.Default;
        }

        public static void LoadThemes()
        {
            if (!ApplicationData.Current.LocalSettings.Containers.ContainsKey("Default"))
            {
                // Default
                SetThemeProperty(Themes.Default, "Accent", "#99173C");
                SetThemeProperty(Themes.Default, "Background", "#2E2633");
                SetThemeProperty(Themes.Default, "Foreground", "#EFFFCD");
                SetThemeProperty(Themes.Default, "DisabledForeground", "#DCE9BE");
                SetThemeProperty(Themes.Default, "Chrome", "#555152");

                // Adrift in Dreams
                SetThemeProperty(Themes.AdriftinDreams, "Accent", "#CFF09E");
                SetThemeProperty(Themes.AdriftinDreams, "Background", "#3B8686");
                SetThemeProperty(Themes.AdriftinDreams, "Foreground", "#A8DBA8");
                SetThemeProperty(Themes.AdriftinDreams, "DisabledForeground", "#0B486B");
                SetThemeProperty(Themes.AdriftinDreams, "Chrome", "#79BD9A");

                // w o r d l e s s .
                SetThemeProperty(Themes.Wordless, "Accent", "#CBE86B");
                SetThemeProperty(Themes.Wordless, "Background", "#1C140D");
                SetThemeProperty(Themes.Wordless, "Foreground", "#FFFFFF");
                SetThemeProperty(Themes.Wordless, "DisabledForeground", "#F2E9E1");
                SetThemeProperty(Themes.Wordless, "Chrome", "#CBE86B");

                // dream magnet
                SetThemeProperty(Themes.DreamMagnet, "Accent", "#00DFFC");
                SetThemeProperty(Themes.DreamMagnet, "Background", "#343838");
                SetThemeProperty(Themes.DreamMagnet, "Foreground", "#00B4CC");
                SetThemeProperty(Themes.DreamMagnet, "DisabledForeground", "#008C9E");
                SetThemeProperty(Themes.DreamMagnet, "Chrome", "#005F6B");

                // coup de grâce
                SetThemeProperty(Themes.CoupDeGrace, "Accent", "#99B898");
                SetThemeProperty(Themes.CoupDeGrace, "Background", "#2A363B");
                SetThemeProperty(Themes.CoupDeGrace, "Foreground", "#FECEA8");
                SetThemeProperty(Themes.CoupDeGrace, "DisabledForeground", "#FF847C");
                SetThemeProperty(Themes.CoupDeGrace, "Chrome", "#E84A5F");

                // Newly Risen Moon
                SetThemeProperty(Themes.NewlyRisenMoon, "Accent", "#EEE6AB");
                SetThemeProperty(Themes.NewlyRisenMoon, "Background", "#36393B");
                SetThemeProperty(Themes.NewlyRisenMoon, "Foreground", "#C5BC8E");
                SetThemeProperty(Themes.NewlyRisenMoon, "DisabledForeground", "#696758");
                SetThemeProperty(Themes.NewlyRisenMoon, "Chrome", "#45484B");

                // Machu Picchu
                SetThemeProperty(Themes.MachuPicchu, "Accent", "#604848");
                SetThemeProperty(Themes.MachuPicchu, "Background", "#607848");
                SetThemeProperty(Themes.MachuPicchu, "Foreground", "#F0F0D8");
                SetThemeProperty(Themes.MachuPicchu, "DisabledForeground", "#C0D860");
                SetThemeProperty(Themes.MachuPicchu, "Chrome", "#789048");

                // Giant Goldfish
                SetThemeProperty(Themes.GiantGoldfish, "Accent", "#FA6900");
                SetThemeProperty(Themes.GiantGoldfish, "Background", "#69D2E7");
                SetThemeProperty(Themes.GiantGoldfish, "Foreground", "#E0E4CC");
                SetThemeProperty(Themes.GiantGoldfish, "DisabledForeground", "#A7DBD8");
                SetThemeProperty(Themes.GiantGoldfish, "Chrome", "#F38630");

                // Thought Provoking
                SetThemeProperty(Themes.ThoughtProvoking, "Accent", "#53777A");
                SetThemeProperty(Themes.ThoughtProvoking, "Background", "#542437");
                SetThemeProperty(Themes.ThoughtProvoking, "Foreground", "#ECD078");
                SetThemeProperty(Themes.ThoughtProvoking, "DisabledForeground", "#D95B43");
                SetThemeProperty(Themes.ThoughtProvoking, "Chrome", "#C02942");

                // Terra
                SetThemeProperty(Themes.Terra, "Accent", "#CDB380");
                SetThemeProperty(Themes.Terra, "Background", "#031634");
                SetThemeProperty(Themes.Terra, "Foreground", "#E8DDCB");
                SetThemeProperty(Themes.Terra, "DisabledForeground", "#036564");
                SetThemeProperty(Themes.Terra, "Chrome", "#033649");

                // mellon ball surprise
                SetThemeProperty(Themes.mellonballsurprise, "Accent", "#F56991");
                SetThemeProperty(Themes.mellonballsurprise, "Background", "#FF9F80");
                SetThemeProperty(Themes.mellonballsurprise, "Foreground", "#D1F2A5");
                SetThemeProperty(Themes.mellonballsurprise, "DisabledForeground", "#FFC48C");
                SetThemeProperty(Themes.mellonballsurprise, "Chrome", "#EFFAB4");

                // Howl's Moving Castle
                SetThemeProperty(Themes.HowlsMovingCastle, "Accent", "#8F7964");
                SetThemeProperty(Themes.HowlsMovingCastle, "Background", "#664F59");
                SetThemeProperty(Themes.HowlsMovingCastle, "Foreground", "#8D98A6");
                SetThemeProperty(Themes.HowlsMovingCastle, "DisabledForeground", "#4B515E");
                SetThemeProperty(Themes.HowlsMovingCastle, "Chrome", "#494A4D");

                // lucky bubble gum
                SetThemeProperty(Themes.luckybubblegum, "Accent", "#E33258");
                SetThemeProperty(Themes.luckybubblegum, "Background", "#170409");
                SetThemeProperty(Themes.luckybubblegum, "Foreground", "#CCBF82");
                SetThemeProperty(Themes.luckybubblegum, "DisabledForeground", "#B8AF03");
                SetThemeProperty(Themes.luckybubblegum, "Chrome", "#67917A");

                // pluck off
                SetThemeProperty(Themes.pluckoff, "Accent", "#8C873E");
                SetThemeProperty(Themes.pluckoff, "Background", "#4D3339");
                SetThemeProperty(Themes.pluckoff, "Foreground", "#D1C5A5");
                SetThemeProperty(Themes.pluckoff, "DisabledForeground", "#793A57");
                SetThemeProperty(Themes.pluckoff, "Chrome", "#A38A5F");

                // french kiss
                SetThemeProperty(Themes.frenchkiss, "Accent", "#B3204D");
                SetThemeProperty(Themes.frenchkiss, "Background", "#151101");
                SetThemeProperty(Themes.frenchkiss, "Foreground", "#EDF6EE");
                SetThemeProperty(Themes.frenchkiss, "DisabledForeground", "#D1C089");
                SetThemeProperty(Themes.frenchkiss, "Chrome", "#412E28");

                // A Dream in Color
                SetThemeProperty(Themes.ADreaminColor, "Accent", "#BEF202");
                SetThemeProperty(Themes.ADreaminColor, "Background", "#1B676B");
                SetThemeProperty(Themes.ADreaminColor, "Foreground", "#88C425");
                SetThemeProperty(Themes.ADreaminColor, "DisabledForeground", "#4B515E");
                SetThemeProperty(Themes.ADreaminColor, "Chrome", "#519548");

                // Windows Phone (Dark)
                SetThemeProperty(Themes.WindowsPhoneDark, "Accent", Windows.UI.Xaml.Application.Current.Resources["PhoneAccentColor"].ToString());
                SetThemeProperty(Themes.WindowsPhoneDark, "Background", "#FF000000");
                SetThemeProperty(Themes.WindowsPhoneDark, "Foreground", "#FFFFFFFF");
                SetThemeProperty(Themes.WindowsPhoneDark, "DisabledForeground", "#99FFFFFF");
                SetThemeProperty(Themes.WindowsPhoneDark, "Chrome", "#FF1F1F1F");
            }
        }

        public static Theme GetTheme(Themes theme)
        {
            return new Theme()
            {
                Accent = GetThemeProperty(theme, "Accent"),
                Background = GetThemeProperty(theme, "Background"),
                Foreground = GetThemeProperty(theme, "Foreground"),
                DisabledForeground = GetThemeProperty(theme, "DisabledForeground"),
                Chrome = GetThemeProperty(theme, "Chrome")
            };
        }

        public static void SetThemeProperty(Themes theme, string property, string value)
        {
            if (!ApplicationData.Current.LocalSettings.Containers.ContainsKey(theme.ToString()))
                ApplicationData.Current.LocalSettings.CreateContainer(theme.ToString(), ApplicationDataCreateDisposition.Always);

            var container = ApplicationData.Current.LocalSettings.Containers[theme.ToString()].Values;

            if (container.ContainsKey(property)) 
                container[property] = value;
            else container.Add(property, value);
        }

        public static string GetThemeProperty(Themes theme, string property)
        {
            if (!ApplicationData.Current.LocalSettings.Containers.ContainsKey(theme.ToString()))
                ApplicationData.Current.LocalSettings.CreateContainer(theme.ToString(), ApplicationDataCreateDisposition.Always);

            var container = ApplicationData.Current.LocalSettings.Containers[theme.ToString()].Values;

            if (container.ContainsKey(property)) 
                return container[property] as string;
            else return null;
        }
    }

    public class Theme
    {
        public string Accent { get; set; }
        public string Background { get; set; }
        public string Foreground { get; set; }
        public string DisabledForeground { get; set; }
        public string Chrome { get; set; }
    }
}
