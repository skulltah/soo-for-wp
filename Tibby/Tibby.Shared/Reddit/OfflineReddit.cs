﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Snoo.Reddit.DataTypes;
using Snoo.Reddit.DataTypes.Prefixes;
using Windows.Data.Xml.Dom;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Snoo.Reddit
{
    public class OfflineReddit
    {
        public static async Task<Subreddits> GetSubreddits()
        {
            try
            {
                var FileNameNews = "Subreddits.xml";
                DataContractSerializer serializer = new DataContractSerializer(typeof(Subreddits));
                var localFolder = ApplicationData.Current.LocalFolder;

                var newsFile = await localFolder.GetFileAsync(FileNameNews);

                IInputStream sessionInputStream = await newsFile.OpenReadAsync();

                return (Subreddits)serializer.ReadObject(sessionInputStream.AsStreamForRead());
            }
            catch { return null; }
        }

        public static async void SetSubreddits(Subreddits subreddits)
        {
            var FileName = "Subreddits.xml";
            DataContractSerializer serializer = new DataContractSerializer(typeof(Subreddits));

            var localFolder = ApplicationData.Current.LocalFolder;
            var file = await localFolder.CreateFileAsync(FileName, CreationCollisionOption.ReplaceExisting);
            IRandomAccessStream sessionRandomAccess = await file.OpenAsync(FileAccessMode.ReadWrite);
            IOutputStream sessionOutputStream = sessionRandomAccess.GetOutputStreamAt(0);
            serializer.WriteObject(sessionOutputStream.AsStreamForWrite(), subreddits);
        }

        public static async Task<List<t5_>> GetRecentSubreddits()
        {
            try
            {
                var FileNameNews = "RecentSubreddits.xml";
                DataContractSerializer serializer = new DataContractSerializer(typeof(List<t5_>));
                var localFolder = ApplicationData.Current.LocalFolder;

                var newsFile = await localFolder.GetFileAsync(FileNameNews);

                IInputStream sessionInputStream = await newsFile.OpenReadAsync();

                var result = (List<t5_>)serializer.ReadObject(sessionInputStream.AsStreamForRead());

                sessionInputStream.Dispose();

                return result;
            }
            catch { return null; }
        }

        public static async Task<List<t5_>> AddRecentSubreddit(t5_ subreddit)
        {
            var data = await GetRecentSubreddits();

            if (subreddit.URL != "/")
            {
                if (data == null)
                    data = new List<t5_>();

                data.Reverse(0, data.Count);

                try
                {
                    var index = data.FindIndex(x => x.URL == subreddit.URL);
                    var item = data[index];
                    data[index] = data[data.Count - 1];
                    data[data.Count - 1] = item;
                }
                catch
                {
                    data.Add(subreddit);
                }

                if (data.Count > 9)
                    data.RemoveRange(9, data.Count);

                data.Reverse(0, data.Count);

                var FileName = "RecentSubreddits.xml";
                DataContractSerializer serializer = new DataContractSerializer(typeof(List<t5_>));

                var localFolder = ApplicationData.Current.LocalFolder;
                var file = await localFolder.CreateFileAsync(FileName, CreationCollisionOption.ReplaceExisting);
                IRandomAccessStream sessionRandomAccess = await file.OpenAsync(FileAccessMode.ReadWrite);
                IOutputStream sessionOutputStream = sessionRandomAccess.GetOutputStreamAt(0);
                serializer.WriteObject(sessionOutputStream.AsStreamForWrite(), data);

                sessionOutputStream.Dispose();
                sessionRandomAccess.Dispose();
            }

            return data;
        }
    }
}
