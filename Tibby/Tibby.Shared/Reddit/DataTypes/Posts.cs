﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Snoo.Reddit.DataTypes.Prefixes;
using System.Collections.ObjectModel;

namespace Snoo.Reddit.DataTypes
{
    public partial class Posts
    {
        public class MediaEmbed
        {

            [JsonProperty("content")]
            public string Content { get; set; }

            [JsonProperty("width")]
            public int? Width { get; set; }

            [JsonProperty("scrolling")]
            public bool? Scrolling { get; set; }

            [JsonProperty("height")]
            public int? Height { get; set; }
        }
    }

    public partial class Posts
    {
        public class Oembed
        {

            [JsonProperty("provider_url")]
            public string ProviderUrl { get; set; }

            [JsonProperty("description")]
            public string Description { get; set; }

            [JsonProperty("title")]
            public string Title { get; set; }

            [JsonProperty("url")]
            public string Url { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("author_name")]
            public string AuthorName { get; set; }

            [JsonProperty("height")]
            public int Height { get; set; }

            [JsonProperty("width")]
            public int Width { get; set; }

            [JsonProperty("html")]
            public string Html { get; set; }

            [JsonProperty("thumbnail_width")]
            public int ThumbnailWidth { get; set; }

            [JsonProperty("version")]
            public string Version { get; set; }

            [JsonProperty("provider_name")]
            public string ProviderName { get; set; }

            [JsonProperty("thumbnail_url")]
            public string ThumbnailUrl { get; set; }

            [JsonProperty("thumbnail_height")]
            public int ThumbnailHeight { get; set; }

            [JsonProperty("author_url")]
            public string AuthorUrl { get; set; }
        }
    }

    public partial class Posts
    {
        public class Media
        {
            [JsonProperty("oembed")]
            public Oembed Oembed { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }
        }
    }

    public partial class Posts
    {
        public class Child
        {
            [JsonProperty("kind")]
            public string Kind { get; set; }

            [JsonProperty("data")]
            public t3_ Data { get; set; }
        }
    }

    public partial class Posts
    {
        public class Data2
        {
            [JsonProperty("modhash")]
            public string Modhash { get; set; }

            [JsonProperty("children")]
            public ObservableCollection<Child> Children { get; set; }

            [JsonProperty("after")]
            public string After { get; set; }

            [JsonProperty("before")]
            public object Before { get; set; }
        }
    }

    public partial class Posts
    {

        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("data")]
        public Data2 Data { get; set; }
    }
}
