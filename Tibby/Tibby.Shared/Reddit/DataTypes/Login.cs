﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snoo.Reddit.DataTypes
{
    public class Login_Data
    {
        public string modhash { get; set; }
        public string cookie { get; set; }
    }

    public class Login_Json
    {
        public List<object> errors { get; set; }
        public Login_Data data { get; set; }
    }

    public class Login_RootObject
    {
        public Login_Json json { get; set; }
    }
}
