﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Snoo.Reddit.DataTypes.Prefixes;

namespace Snoo.Reddit.DataTypes
{
    public class MediaEmbed
    {
    }

    public class SecureMediaEmbed
    {
    }

    public class Child
    {
        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("data")]
        public t1_ Data { get; set; }
    }

    public class Data2
    {
        [JsonProperty("modhash")]
        public string Modhash { get; set; }

        [JsonProperty("children")]
        public IList<Child> Children { get; set; }

        [JsonProperty("after")]
        public object After { get; set; }

        [JsonProperty("before")]
        public object Before { get; set; }
    }


    public class Comments
    {
        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("data")]
        public Data2 Data { get; set; }
    }
}
