﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snoo.Reddit.DataTypes
{
    public partial class Me
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("created")]
        public double Created { get; set; }

        [JsonProperty("gold_creddits")]
        public int GoldCreddits { get; set; }

        [JsonProperty("created_utc")]
        public double CreatedUtc { get; set; }

        [JsonProperty("link_karma")]
        public int LinkKarma { get; set; }

        [JsonProperty("comment_karma")]
        public int CommentKarma { get; set; }

        [JsonProperty("over_18")]
        public bool Over18 { get; set; }

        [JsonProperty("is_gold")]
        public bool IsGold { get; set; }

        [JsonProperty("is_mod")]
        public bool IsMod { get; set; }

        [JsonProperty("gold_expiration")]
        public object GoldExpiration { get; set; }

        [JsonProperty("hasverified_email")]
        public object HasverifiedEmail { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
