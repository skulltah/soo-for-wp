﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Snoo.Reddit.DataTypes.Prefixes;
using System.Collections.ObjectModel;

namespace Snoo.Reddit.DataTypes
{
    public partial class Subreddits
    {
        public class Child
        {

            [JsonProperty("kind")]
            public string Kind { get; set; }

            [JsonProperty("data")]
            public t5_ Data { get; set; }
        }
    }

    public partial class Subreddits
    {
        public class Data2
        {
            [JsonProperty("modhash")]
            public string Modhash { get; set; }

            [JsonProperty("children")]
            public ObservableCollection<Child> Children { get; set; }

            [JsonProperty("after")]
            public string After { get; set; }

            [JsonProperty("before")]
            public object Before { get; set; }
        }
    }

    public partial class Subreddits
    {

        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("data")]
        public Data2 Data { get; set; }
    }
}
