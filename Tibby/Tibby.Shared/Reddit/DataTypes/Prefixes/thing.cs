﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snoo.Reddit.DataTypes.Prefixes
{
    /// <summary>
    /// Thing (reddit base class)
    /// </summary>
    public class thing
    {
        /// <summary>
        /// this item's identifier, e.g. "8xwlg"
        /// </summary>
        [JsonProperty("id")]
        public string ID { get; set; }

        /// <summary>
        /// Fullname of comment, e.g. "t1_c3v7f8u"
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// All things have a kind. The kind is a String identifier that denotes the object's type. Some examples: Listing, more, t1, t2
        /// </summary>
        [JsonProperty("kind")]
        public string Kind { get; set; }

        /// <summary>
        /// A custom data structure used to hold valuable information. This object's format will follow the data structure respective of its kind. See below for specific structures.
        /// </summary>
        [JsonProperty("data")]
        public object Data { get; set; }
    }
}
