﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snoo.Reddit.DataTypes.Prefixes
{
    /// <summary>
    /// Link
    /// </summary>
    public class t3_
    {
        [JsonProperty("domain")]
        public string Domain { get; set; }

        [JsonProperty("banned_by")]
        public string BannedBy { get; set; }

        [JsonProperty("media_embed")]
        public object MediaEmbed { get; set; }

        [JsonProperty("subreddit")]
        public string Subreddit { get; set; }

        [JsonProperty("selftext_html")]
        public string SelftextHtml { get; set; }

        [JsonProperty("selftext")]
        public string Selftext { get; set; }

        [JsonProperty("likes")]
        public string Likes { get; set; }

        [JsonProperty("secure_media")]
        public object SecureMedia { get; set; }

        [JsonProperty("link_flair_text")]
        public string LinkFlairText { get; set; }

        [JsonProperty("id")]
        public string ID { get; set; }

        [JsonProperty("gilded")]
        public int Gilded { get; set; }

        [JsonProperty("secure_media_embed")]
        public object SecureMediaEmbed { get; set; }

        [JsonProperty("clicked")]
        public bool Clicked { get; set; }

        [JsonProperty("sticked")]
        public bool Sticked { get; set; }

        [JsonProperty("author")]
        public string Author { get; set; }

        [JsonProperty("media")]
        public object Media { get; set; }

        [JsonProperty("score")]
        public int Score { get; set; }

        [JsonProperty("approved_by")]
        public string ApprovedBy { get; set; }

        [JsonProperty("over_18")]
        public bool Over18 { get; set; }

        [JsonProperty("hidden")]
        public bool Hidden { get; set; }

        [JsonProperty("thumbnail")]
        public string Thumbnail { get; set; }

        [JsonProperty("subreddit_id")]
        public string SubredditID { get; set; }

        [JsonProperty("edited")]
        public bool Edited { get; set; }

        [JsonProperty("link_flair_css_class")]
        public string LinkFlairCSSClass { get; set; }

        [JsonProperty("author_flair_css_class")]
        public string AuthorFlairCSSClass { get; set; }

        [JsonProperty("downs")]
        public int Downs { get; set; }

        [JsonProperty("saved")]
        public bool Saved { get; set; }

        [JsonProperty("is_self")]
        public bool IsSelf { get; set; }

        [JsonProperty("permalink")]
        public string Permalink { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("created")]
        public double Created { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("author_flair_text")]
        public string AuthorFlairText { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("created_utc")]
        public double CreatedUTC { get; set; }

        [JsonProperty("ups")]
        public int Ups { get; set; }

        [JsonProperty("num_comments")]
        public int NumComments { get; set; }

        [JsonProperty("visited")]
        public bool Visited { get; set; }

        [JsonProperty("num_reports")]
        public int NumReports { get; set; }

        [JsonProperty("distinguished")]
        public bool Distinguished { get; set; }
    }
}
