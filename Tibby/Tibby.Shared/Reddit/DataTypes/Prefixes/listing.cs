﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snoo.Reddit.DataTypes.Prefixes
{
    /// <summary>
    /// Listing
    /// </summary>
    public class listing
    {
        /// <summary>
        /// The fullname of the listing that follows before this page. null if there is no previous page.
        /// </summary>
        [JsonProperty("before")]
        public string Before { get; set; }

        /// <summary>
        /// The fullname of the listing that follows after this page. null if there is no next page.
        /// </summary>
        [JsonProperty("after")]
        public string After { get; set; }

        /// <summary>
        /// This modhash is not the same modhash provided upon login. You do not need to update your user's modhash everytime you get a new modhash. You can reuse the modhash given upon login.
        /// </summary>
        [JsonProperty("modhash")]
        public string Modhash { get; set; }

        /// <summary>
        /// A list of things that this Listing wraps.
        /// </summary>
        [JsonProperty("data")]
        public List<thing> Data { get; set; }
    }
}
