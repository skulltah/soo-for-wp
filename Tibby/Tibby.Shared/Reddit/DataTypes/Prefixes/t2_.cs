﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snoo.Reddit.DataTypes.Prefixes
{
    /// <summary>
    /// Acount
    /// </summary>
    public class t2_
    {
        /// <summary>
        /// user's comment karma
        /// </summary>
        [JsonProperty("comment_karma")]
        public int CommentKarma { get; set; }

        /// <summary>
        /// user has unread mail? null if not your account
        /// </summary>
        [JsonProperty("has_mail")]
        public bool HasMail { get; set; }

        /// <summary>
        /// user has unread mod mail? null if not your account
        /// </summary>
        [JsonProperty("has_mod_mail")]
        public bool HasModMail { get; set; }

        /// <summary>
        /// user has provided an email address and got it verified?
        /// </summary>
        [JsonProperty("has_verified_email")]
        public bool HasVerifiedEmail { get; set; }

        /// <summary>
        /// ID of the account; prepend t2_ to get fullname
        /// </summary>
        [JsonProperty("id")]
        public string ID { get; set; }

        /// <summary>
        /// whether the logged-in user has this user set as a friend
        /// </summary>
        [JsonProperty("is_friend")]
        public bool IsFriend { get; set; }

        /// <summary>
        /// reddit gold status
        /// </summary>
        [JsonProperty("is_gold")]
        public bool IsGold { get; set; }

        /// <summary>
        /// whether this account moderates any subreddits
        /// </summary>
        [JsonProperty("is_mod")]
        public bool IsMod { get; set; }

        /// <summary>
        /// user's link karma
        /// </summary>
        [JsonProperty("link_karma")]
        public int LinkKarma { get; set; }

        /// <summary>
        /// current modhash. not present if not your account
        /// </summary>
        [JsonProperty("modhash")]
        public string Modhash { get; set; }

        /// <summary>
        /// The username of the account in question. This attribute overrides the superclass's name attribute. Do not confuse an account's name which is the account's username with a thing's name which is the thing's FULLNAME. See API: Glossary for details on what FULLNAMEs are.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// whether this account is set to be over 18
        /// </summary>
        [JsonProperty("over_18")]
        public bool Over18 { get; set; }

        //created

        /// <summary>
        /// the time of creation in local epoch-second format. ex: 1331042771.0
        /// </summary>
        [JsonProperty("created")]
        public long Created { get; set; }

        /// <summary>
        /// the time of creation in UTC epoch-second format. Note that neither of these ever have a non-zero fraction.
        /// </summary>
        [JsonProperty("created_utc")]
        public long CreatedUTC { get; set; }


// {
//    "kind": "t2", 
//    "data": {
//        "has_mail": false, 
//        "name": "fooBar", 
//        "created": 123456789.0, 
//        "modhash": "f0f0f0f0f0f0f0f0...", 
//        "created_utc": 1315269998.0, 
//        "link_karma": 31, 
//        "comment_karma": 557, 
//        "is_gold": false, 
//        "is_mod": false, 
//        "has_verified_email": false, 
//        "id": "5sryd", 
//        "has_mod_mail": false
//    }
// }
    }
}
