﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snoo.Reddit.DataTypes.Prefixes
{
    /// <summary>
    /// Message
    /// </summary>
    public class t4_
    {
        [JsonProperty("author")]
        public string Author { get; set; }

        /// <summary>
        /// the message itself
        /// </summary>
        [JsonProperty("body")]
        public string Body { get; set; }

        /// <summary>
        /// the message itself with HTML formatting
        /// </summary>
        [JsonProperty("body_html")]
        public string BodyHTML { get; set; }

        /// <summary>
        /// does not seem to return null but an empty string instead.
        /// </summary>
        [JsonProperty("context")]
        public string Context { get; set; }

        /// <summary>
        /// either null or the first message's ID represented as base 10 (wtf)
        /// </summary>
        [JsonProperty("first_message")]
        public string FirstMessage { get; set; }

        /// <summary>
        /// how the logged-in user has voted on the message - True = upvoted, False = downvoted, null = no vote
        /// </summary>
        [JsonProperty("likes")]
        public string Likes { get; set; }

        /// <summary>
        /// if the message is actually a comment, contains the title of the thread it was posted in
        /// </summary>
        [JsonProperty("link_title")]
        public string LinkTitle { get; set; }

        /// <summary>
        /// ex: "t4_8xwlg"
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// unread? not sure
        /// </summary>
        [JsonProperty("new")]
        public string New { get; set; }

        /// <summary>
        /// null if no parent is attached
        /// </summary>
        [JsonProperty("parent_id")]
        public string ParentID { get; set; }

        /// <summary>
        /// Again, an empty string if there are no replies.
        /// </summary>
        [JsonProperty("replies")]
        public string Replies { get; set; }

        /// <summary>
        /// subject of message
        /// </summary>
        [JsonProperty("subject")]
        public string Subject { get; set; }

        /// <summary>
        /// null if not a comment.
        /// </summary>
        [JsonProperty("subreddit")]
        public string Subreddit { get; set; }

        [JsonProperty("was_comment")]
        public string WasComment { get; set; }

        //created

        /// <summary>
        /// the time of creation in local epoch-second format. ex: 1331042771.0
        /// </summary>
        [JsonProperty("created")]
        public long Created { get; set; }

        /// <summary>
        /// the time of creation in UTC epoch-second format. Note that neither of these ever have a non-zero fraction.
        /// </summary>
        [JsonProperty("created_utc")]
        public long CreatedUTC { get; set; }
    }
}
