﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snoo.Reddit.DataTypes.Prefixes
{
    /// <summary>
    /// Comment
    /// </summary>
    public class t1_
    {
        /// <summary>
        /// who approved this comment. null if nobody or you are not a mod
        /// </summary>
        [JsonProperty("approved_by")]
        public string ApprovedBy { get; set; }

        /// <summary>
        /// the account name of the poster
        /// </summary>
        [JsonProperty("author")]
        public string Author { get; set; }

        /// <summary>
        /// the CSS class of the author's flair. subreddit specific
        /// </summary>
        [JsonProperty("author_flair_css_class")]
        public string AuthorFlairCSSClass { get; set; }

        /// <summary>
        /// the text of the author's flair. subreddit specific
        /// </summary>
        [JsonProperty("author_flair_text")]
        public string AuthorFlairText { get; set; }

        /// <summary>
        /// who removed this comment. null if nobody or you are not a mod
        /// </summary>
        [JsonProperty("banned_by")]
        public string BannedBy { get; set; }

        /// <summary>
        /// the raw text. this is the unformatted text which includes the raw markup characters such as ** for bold. <, >, and & are escaped.
        /// </summary>
        [JsonProperty("body")]
        public string Body { get; set; }

        /// <summary>
        /// the formatted HTML text as displayed on reddit. For example, text that is emphasised by * will now have <em> tags wrapping it. Additionally, bullets and numbered lists will now be in HTML list format. NOTE: The HTML string will be escaped. You must unescape to get the raw HTML.
        /// </summary>
        [JsonProperty("body_html")]
        public string BodyHTML { get; set; }

        /// <summary>
        /// false if not edited, edit date in UTC epoch-seconds otherwise. NOTE: for some old edited comments on reddit.com, this will be set to true instead of edit date.
        /// </summary>
        [JsonProperty("edited")]
        public string Edited { get; set; }

        /// <summary>
        /// the number of times this comment received reddit gold
        /// </summary>
        [JsonProperty("gilded")]
        public int Gilded { get; set; }

        /// <summary>
        /// how the logged-in user has voted on the comment - True = upvoted, False = downvoted, null = no vote
        /// </summary>
        [JsonProperty("likes")]
        public string Likes { get; set; }

        /// <summary>
        /// present if the comment is being displayed outside its thread (user pages, /r/subreddit/comments/.json, etc.). Contains the author of the parent link
        /// </summary>
        [JsonProperty("link_author")]
        public string LinkAuthor { get; set; }

        /// <summary>
        /// ID of the link this comment is in
        /// </summary>
        [JsonProperty("link_id")]
        public string LinkID { get; set; }

        /// <summary>
        /// present if the comment is being displayed outside its thread (user pages, /r/subreddit/comments/.json, etc.). Contains the title of the parent link
        /// </summary>
        [JsonProperty("link_title")]
        public string LinkTitle { get; set; }

        /// <summary>
        /// present if the comment is being displayed outside its thread (user pages, /r/subreddit/comments/.json, etc.). Contains the URL of the parent link
        /// </summary>
        [JsonProperty("link_url")]
        public string LinkURL { get; set; }

        /// <summary>
        /// how many times this comment has been reported, null if not a mod
        /// </summary>
        [JsonProperty("num_reports")]
        public int NumReports { get; set; }

        /// <summary>
        /// ID of the thing this comment is a reply to, either the link or a comment in it
        /// </summary>
        [JsonProperty("parent_id")]
        public string ParentID { get; set; }

        /// <summary>
        /// true if this post is saved by the logged in user
        /// </summary>
        [JsonProperty("saved")]
        public bool Saved { get; set; }

        /// <summary>
        /// Whether the comment's score is currently hidden.
        /// </summary>
        [JsonProperty("score_hidden")]
        public bool ScoreHidden { get; set; }

        /// <summary>
        /// subreddit of thing excluding the /r/ prefix. "pics"
        /// </summary>
        [JsonProperty("subreddit")]
        public string Subreddit { get; set; }

        /// <summary>
        /// the id of the subreddit which is the thing is located in
        /// </summary>
        [JsonProperty("subreddit_id")]
        public string SubredditId { get; set; }

        /// <summary>
        /// to allow determining whether they have been distinguished by moderators/admins. null = not distinguished. moderator = the green [M]. admin = the red [A]. special = various other special distinguishes http://bit.ly/ZYI47B
        /// </summary>
        [JsonProperty("distinguished")]
        public string Distinguished { get; set; }

        //votable

        /// <summary>
        /// the number of upvotes. (includes own)
        /// </summary>
        [JsonProperty("ups")]
        public int Ups { get; set; }

        /// <summary>
        /// the number of downvotes. (includes own)
        /// </summary>
        [JsonProperty("downs")]
        public int Downs { get; set; }

        //created

        /// <summary>
        /// the time of creation in local epoch-second format. ex: 1331042771.0
        /// </summary>
        [JsonProperty("created")]
        public long Created { get; set; }

        /// <summary>
        /// the time of creation in UTC epoch-second format. Note that neither of these ever have a non-zero fraction.
        /// </summary>
        [JsonProperty("created_utc")]
        public long CreatedUTC { get; set; }

        //misc

        [JsonProperty("replies")]
        public Comments Replies { get; set; }

        [JsonProperty("id")]
        public string ID { get; set; }

        [JsonProperty("score")]
        public int Score { get; set; }

        [JsonProperty("controversiality")]
        public int Controversiality { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
