﻿// Generated by Xamasoft JSON Class Generator
// http://www.xamasoft.com/json-class-generator

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Snoo.Reddit.DataTypes.Prefixes;
using System.Collections.ObjectModel;

namespace Snoo
{

    public partial class UserComments
    {
        public class Child
        {
            [JsonProperty("kind")]
            public string Kind { get; set; }

            [JsonProperty("data")]
            public t1_ Data { get; set; }
        }
    }

    public partial class UserComments
    {
        public class Data2
        {
            [JsonProperty("modhash")]
            public string Modhash { get; set; }

            [JsonProperty("children")]
            public ObservableCollection<Child> Children { get; set; }

            [JsonProperty("after")]
            public string After { get; set; }

            [JsonProperty("before")]
            public object Before { get; set; }
        }
    }

    public partial class UserComments
    {
        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("data")]
        public Data2 Data { get; set; }
    }

}
