﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snoo.Reddit
{
    public class URIs
    {
        public static readonly string Base = "http://www.reddit.com{0}";
        public static readonly string SSLBase = "https://ssl.reddit.com{0}";
        public static readonly string APIBase = "http://api.reddit.com{0}";

        public static readonly string Login = String.Format(SSLBase, "/api/login");
        public static readonly string DefaultSubreddits = String.Format(APIBase, "/reddits.json");
        public static readonly string SubscribedSubreddits = "/subreddits/mine/subscriber.json?limit={0}";
        public static readonly string SubredditPosts = "{0}{1}?t={2}&after={3}&before={4}&count={5}limit={6}";
        public static readonly string Profile = "/user/{0}/about.json";
        public static readonly string UserSubmissions = "/user/{0}/submitted.json";
        public static readonly string UserComments = "/user/{0}/comments.json";
        public static readonly string PopularSubreddits = "/subreddits/popular.json";
        public static readonly string SearchPosts = "/subreddits/popular.json";
        public static readonly string SearchSubreddits = "/subreddits/search.json?after={0}&before={1}&count={2}&limit={3}&q={4}&show=all";
        public static readonly string Search = "/search.json?after={0}&before={1}&count={2}&limit={3}&restrict_sr={4}&q={5}&sort=relevance&syntax=plain&t=all";
        public static readonly string Comments = "/r/{0}/comments/{1}.json";
        public static readonly string Friend = "/api/friend";
        public static readonly string Unfriend = "/api/unfriend";
        public static readonly string Subscribe = "/api/subscribe";
        public static readonly string SearchSubreddit = "{0}search.json?after={1}&before={2}&count={3}&limit={4}&restrict_sr={5}&q={6}&sort=relevance&syntax=plain&t=all";
        public static readonly string Register = "/api/register";
        public static readonly string GetComments = "/comments/{0}.json";
        public static readonly string Save = "/api/save";
        public static readonly string Unsave = "/api/unsave";
        public static readonly string Vote = "/api/vote";
        public static readonly string Comment = "/api/comment";
        public static readonly string Post = "/api/submit";

        public static readonly string NeedsCaptcha = "/api/needs_captcha.json";
        public static readonly string NewCaptcha = "/api/new_captcha";
        public static readonly string GetCaptcha = "/captcha/{0}";

    }
}
