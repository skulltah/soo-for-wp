﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Snoo.Reddit;
using Snoo.Reddit.DataTypes;
using Windows.UI.Popups;

namespace Snoo.Reddit
{
    public class Reddit
    {
        public enum SubredditListing
        {
            Hot,
            New,
            Top,
            Controversial
        }

        public enum PostKind
        {
            link,
            self
        }

        public static async Task<string> PostAsync(string url, List<KeyValuePair<string, string>> parameters)
        {
            var baseAddress = new Uri("http://reddit.com");
            using (var handler = new HttpClientHandler { UseCookies = false })
            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                client.DefaultRequestHeaders.Add("user-agent", "SNUWU/0.1 by marsmax");
                client.DefaultRequestHeaders.Add("Cookie", "reddit_session=" + WebUtility.UrlEncode(LoginHelper.GetCookie()));
                var response = await client.PostAsync(url, new FormUrlEncodedContent(parameters));
                var responseString = await response.Content.ReadAsStringAsync();

                if (Debugger.IsAttached)
                    Debug.WriteLine("response: " + responseString);

                if (String.Compare(responseString, "{}", StringComparison.OrdinalIgnoreCase) == -89)
                    return null;

                if (responseString.ToLower().Contains("please login"))
                {
                    var msg = new MessageDialog("Some of the features of reddit require you to login, you can do that from the main page.", "Please login");
                    await msg.ShowAsync();
                }

                return responseString;
            }
        }

        public static async Task<string> GetAsync(string url)
        {
            var baseAddress = new Uri("http://reddit.com");
            using (var handler = new HttpClientHandler { UseCookies = false })
            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                client.DefaultRequestHeaders.Add("user-agent", "SNUWU/0.1 by marsmax");
                var message = new HttpRequestMessage(HttpMethod.Get, url);
                message.Headers.Add("Cookie", "reddit_session=" + WebUtility.UrlEncode(LoginHelper.GetCookie()));
                var response = await client.SendAsync(message);
                var responseString = await response.Content.ReadAsStringAsync();

                if (Debugger.IsAttached)
                    Debug.WriteLine("response: " + responseString);

                if (String.Compare(responseString, "{}", StringComparison.OrdinalIgnoreCase) == -89)
                {
                    ErrorHandler.ResponseIsNull();
                    return null;
                }

                if (responseString.ToLower().Contains("please login"))
                {
                    var msg = new MessageDialog("Some of the features of reddit require you to login, you can do that from the main page.", "Please login");
                    await msg.ShowAsync();
                }

                return responseString;
            }
        }

        /// <summary>
        /// Gets a list of default subreddits
        /// </summary>
        /// <returns>List of subreddits</returns>
        public static async Task<Subreddits> GetDefaultSubreddits()
        {
            var url = URIs.DefaultSubreddits;

            var json = await GetAsync(url);

            if (json == null)
                return null;

            return JsonConvert.DeserializeObject<Subreddits>(json, new MyConverter());
        }

        /// <summary>
        /// Gets a list of subreddits the user is subscribed to
        /// </summary>
        /// <param name="limit">Max number of results to return (ex: 20)</param>
        /// <returns>List of subreddits</returns>
        public static async Task<Subreddits> GetSubreddits(int limit)
        {
            var url = string.Format(URIs.SubscribedSubreddits, limit.ToString());

            var json = await GetAsync(url);

            if (json == null) 
                return null;

            return JsonConvert.DeserializeObject<Subreddits>(json, new MyConverter());
        }

        public static async Task<Posts> GetPosts(string subredditurl, SubredditListing listing, string t, string after, string before, int count)
        {
            var url = string.Format(URIs.APIBase, string.Format(URIs.SubredditPosts, subredditurl, listing.ToString().ToLower(), t, after, before, count, (double)Settings.GetSetting("ListingLimit")));
            var json = await GetAsync(url);
            Debug.WriteLine(json);
            if (json == null)
                return null;

            return JsonConvert.DeserializeObject<Posts>(json, new MyConverter());
        }

        public static async Task<Profile> GetProfile(string username)
        {
            var url = string.Format(URIs.APIBase, string.Format(URIs.Profile, username));
            var json = await GetAsync(url);
            if (json == null)
                return null;

            return JsonConvert.DeserializeObject<Profile>(json, new MyConverter());
        }

        public static async Task<Posts> GetUserSubmissions(string username)
        {
            var url = string.Format(URIs.APIBase, string.Format(URIs.UserSubmissions, username));
            var json = await GetAsync(url);
            if (json == null)
                return null;

            return JsonConvert.DeserializeObject<Posts>(json, new MyConverter());
        }

        public static async Task<UserComments> GetUserComments(string username)
        {
            var url = string.Format(URIs.APIBase, string.Format(URIs.UserComments, username));
            var json = await GetAsync(url);
            if (json == null)
                return null;

            return JsonConvert.DeserializeObject<UserComments>(json, new MyConverter());
        }

        public static async Task<Subreddits> GetPopularSubreddits()
        {
            var url = string.Format(URIs.APIBase, URIs.PopularSubreddits);
            var json = await GetAsync(url);
            if (json == null)
                return null;

            return JsonConvert.DeserializeObject<Subreddits>(json, new MyConverter());
        }

        public static async Task<Subreddits> SearchSubreddits(string before, string after, int count, string query)
        {
            var url = string.Format(URIs.APIBase, string.Format(URIs.SearchSubreddits, before, after, count.ToString(), (double)Settings.GetSetting("ListingLimit"), query));
            var json = await GetAsync(url);
            if (json == null)
                return null;

            return JsonConvert.DeserializeObject<Subreddits>(json, new MyConverter());
        }

        public static async Task<SearchResults> Search(string before, string after, int count, string query, bool restricted)
        {
            var url = string.Format(URIs.APIBase, string.Format(URIs.Search, after, before, count.ToString(), (double)Settings.GetSetting("ListingLimit"), restricted.ToString(), query));
            var json = await GetAsync(url);
            if (json == null)
                return null;

            return JsonConvert.DeserializeObject<SearchResults>(json, new MyConverter());
        }

        public static async Task<List<Comments>> GetComments(string subreddit, string id)
        {
            var url = string.Format(URIs.APIBase, string.Format(URIs.Comments, subreddit, id));
            var json = await GetAsync(url);
            if (json == null)
                return null;

            return JsonConvert.DeserializeObject<List<Comments>>(json, new MyConverter());
        }

        public static async Task<bool> Friend(string username)
        {
            try
            {
                var url = string.Format(URIs.APIBase, URIs.Friend);
                var json = await PostAsync(url, new List<KeyValuePair<string, string>>(){
                new KeyValuePair<string, string>("api_type", "json"),
            new KeyValuePair<string, string>("ban_message", ""),
            new KeyValuePair<string, string>("container", ""),
            new KeyValuePair<string, string>("duration", "2"),
            new KeyValuePair<string, string>("name", username),
            new KeyValuePair<string, string>("note", ""),
            new KeyValuePair<string, string>("permissions", ""),
            new KeyValuePair<string, string>("type", "friend"),
            new KeyValuePair<string, string>("uh", LoginHelper.GetModhash())
                });

                return true;
            }
            catch { return false; }
        }

        public static async Task<bool> Unriend(string username)
        {
            try
            {
                var url = string.Format(URIs.APIBase, URIs.Unfriend);
                var json = await PostAsync(url, new List<KeyValuePair<string, string>>(){
                new KeyValuePair<string, string>("api_type", "json"),
                new KeyValuePair<string, string>("container", ""),
            new KeyValuePair<string, string>("id", ""),
            new KeyValuePair<string, string>("name", username),
            new KeyValuePair<string, string>("type", "friend"),
            new KeyValuePair<string, string>("uh", LoginHelper.GetModhash())
                });

                return true;
            }
            catch { return false; }
        }

        public static async Task<bool> Subscribe(string subreddit)
        {
            try
            {
                var url = string.Format(URIs.APIBase, URIs.Subscribe);
                var json = await PostAsync(url, new List<KeyValuePair<string, string>>(){
                new KeyValuePair<string, string>("api_type", "json"),
                new KeyValuePair<string, string>("action", "sub"),
            new KeyValuePair<string, string>("sr", subreddit),
            new KeyValuePair<string, string>("uh", LoginHelper.GetModhash())
                });

                return true;
            }
            catch { return false; }
        }

        public static async Task<bool> Unsubscribe(string subreddit)
        {
            try
            {
                var url = string.Format(URIs.APIBase, URIs.Subscribe);
                var json = await PostAsync(url, new List<KeyValuePair<string, string>>(){
                new KeyValuePair<string, string>("api_type", "json"),
                new KeyValuePair<string, string>("action", "unsub"),
            new KeyValuePair<string, string>("sr", subreddit),
            new KeyValuePair<string, string>("uh", LoginHelper.GetModhash())
                });

                return true;
            }
            catch { return false; }
        }

        public static async Task<SearchResults> SearchSubreddit(string subreddit, string before, string after, int count, string query, bool restricted)
        {
            var url = string.Format(URIs.APIBase, string.Format(URIs.SearchSubreddit, subreddit, after, before, count.ToString(), (double)Settings.GetSetting("ListingLimit"), restricted.ToString(), query));
            var json = await GetAsync(url);
            if (json == null)
                return null;

            return JsonConvert.DeserializeObject<SearchResults>(json, new MyConverter());
        }

        public static async Task<RegisterResult> Register(string captcha, string email, string captchaIden, string password, string username)
        {
            string json = "";

            var url = string.Format(URIs.APIBase, URIs.Register);

            var baseAddress = new Uri("http://reddit.com");
            using (var handler = new HttpClientHandler { UseCookies = false })
            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                client.DefaultRequestHeaders.Add("user-agent", "SNUWU/0.1 by marsmax");
                var response = await client.PostAsync(url, new FormUrlEncodedContent(new List<KeyValuePair<string, string>>(){
                new KeyValuePair<string, string>("api_type", "json"),
                new KeyValuePair<string, string>("captcha", captcha),
                new KeyValuePair<string, string>("email", email),
                new KeyValuePair<string, string>("iden", captchaIden),
                new KeyValuePair<string, string>("passwd", password),
                new KeyValuePair<string, string>("passwd2", password),
            new KeyValuePair<string, string>("rem", "true"),
            new KeyValuePair<string, string>("user", username)
                }));
                var responseString = await response.Content.ReadAsStringAsync();
                json = responseString;

                Debug.WriteLine(json);
            }
            return JsonConvert.DeserializeObject<RegisterResult>(json, new MyConverter());
        }

        public static async Task<bool> NeedsCaptcha()
        {
            var url = string.Format(URIs.APIBase, URIs.NeedsCaptcha);
            var json = await GetAsync(url);

            return Helpers.General.StringToBool(json);
        }

        public static async Task<CaptchaRequestResult> GetNewCaptcha()
        {
            try
            {
                var url = string.Format(URIs.APIBase, URIs.NewCaptcha);
                var json = await PostAsync(url, new List<KeyValuePair<string, string>>(){
                new KeyValuePair<string, string>("api_type", "json")
                });

                return JsonConvert.DeserializeObject<CaptchaRequestResult>(json, new MyConverter());
            }
            catch { return null; }
        }

        public static async Task<Windows.UI.Xaml.Media.Imaging.BitmapImage> GetCaptcha(string iden)
        {
            var url = string.Format(URIs.APIBase, string.Format(URIs.GetCaptcha, iden));

            var baseAddress = new Uri("http://reddit.com");
            using (var handler = new HttpClientHandler { UseCookies = false })
            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                client.DefaultRequestHeaders.Add("user-agent", "SNUWU/0.1 by marsmax");
                var message = new HttpRequestMessage(HttpMethod.Get, url);
                message.Headers.Add("Cookie", "reddit_session=" + WebUtility.UrlEncode(LoginHelper.GetCookie()));
                var response = await (await client.SendAsync(message)).Content.ReadAsStreamAsync();

                var bitmap = new Windows.UI.Xaml.Media.Imaging.BitmapImage();
                await bitmap.SetSourceAsync(System.IO.WindowsRuntimeStreamExtensions.AsRandomAccessStream(response));

                return bitmap;
            }
        }

        public static async Task<List<Comments>> GetComments(string id)
        {
            var url = string.Format(URIs.Base, string.Format(URIs.GetComments, id));
            var json = await GetAsync(url);
            if (json == null)
                return null;

            return JsonConvert.DeserializeObject<List<Comments>>(json, new MyConverter());
        }

        public static async Task<bool> Save(string id)
        {
            try
            {
                var url = string.Format(URIs.Base, URIs.Save);
                var json = await PostAsync(url, new List<KeyValuePair<string, string>>(){
                new KeyValuePair<string, string>("api_type", "json"),
                new KeyValuePair<string, string>("category", "Snoo"),
            new KeyValuePair<string, string>("id", id),
            new KeyValuePair<string, string>("uh", LoginHelper.GetModhash())
                });

                return true;
            }
            catch { return false; }
        }

        public static async Task<bool> Unsave(string id)
        {
            try
            {
                var url = string.Format(URIs.Base, URIs.Save);
                var json = await PostAsync(url, new List<KeyValuePair<string, string>>(){
                new KeyValuePair<string, string>("api_type", "json"),
            new KeyValuePair<string, string>("id", id),
            new KeyValuePair<string, string>("uh", LoginHelper.GetModhash())
                });

                return true;
            }
            catch { return false; }
        }

        public static async Task<bool> Vote(int dir, string id)
        {
            try
            {
                var url = string.Format(URIs.Base, URIs.Vote);
                var json = await PostAsync(url, new List<KeyValuePair<string, string>>(){
                new KeyValuePair<string, string>("api_type", "json"),
            new KeyValuePair<string, string>("dir", dir.ToString()),
            new KeyValuePair<string, string>("id", id),
            new KeyValuePair<string, string>("uh", LoginHelper.GetModhash())
                });
                Debug.WriteLine(json);
                return true;
            }
            catch { return false; }
        }

        public static async Task<bool> Comment(string text, string id)
        {
            try
            {
                var url = string.Format(URIs.Base, URIs.Comment);
                var json = await PostAsync(url, new List<KeyValuePair<string, string>>(){
                new KeyValuePair<string, string>("api_type", "json"),
            new KeyValuePair<string, string>("text", text),
            new KeyValuePair<string, string>("thing_id", id),
            new KeyValuePair<string, string>("uh", LoginHelper.GetModhash())
                });

                return true;
            }
            catch { return false; }
        }

        public static async Task<RegisterResult> Post(string captcha, string iden, PostKind kind, bool sendreplies, string sr, string text, string title, string _url)
        {
            try
            {
                var url = string.Format(URIs.APIBase, URIs.Post);
                var json = await PostAsync(url, new List<KeyValuePair<string, string>>(){
                new KeyValuePair<string, string>("api_type", "json"),
            new KeyValuePair<string, string>("captcha", captcha),
            new KeyValuePair<string, string>("extension", ""),
            new KeyValuePair<string, string>("iden", iden),
            new KeyValuePair<string, string>("kind", kind.ToString()),
            new KeyValuePair<string, string>("resubmit", "true"),
            new KeyValuePair<string, string>("save", "false"),
            new KeyValuePair<string, string>("sendreplies", sendreplies.ToString()),
            new KeyValuePair<string, string>("sr", sr),
            new KeyValuePair<string, string>("text", text),
            new KeyValuePair<string, string>("then", "comments"),
            new KeyValuePair<string, string>("title", title),
            new KeyValuePair<string, string>("uh", LoginHelper.GetModhash()),
            new KeyValuePair<string, string>("url", _url)
                });

                return JsonConvert.DeserializeObject<RegisterResult>(json, new MyConverter());;
            }
            catch { return null; }
        }
    }
}
