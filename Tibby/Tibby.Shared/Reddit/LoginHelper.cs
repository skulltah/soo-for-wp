﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Snoo.Reddit.DataTypes;

namespace Snoo.Reddit
{
    public class LoginHelper
    {
        public static async Task<bool> Login(string username, string password)
        {
            try
            {
                var data = new List<KeyValuePair<string, string>>
    {
        new KeyValuePair<string, string>("api_type", "json"),
        new KeyValuePair<string, string>("passwd", password),
        new KeyValuePair<string, string>("rem", "true"),
        new KeyValuePair<string, string>("user", username)
    };

                var httpClient = new HttpClient();
                var response = await httpClient.PostAsync(URIs.Login, new FormUrlEncodedContent(data));
                response.EnsureSuccessStatusCode();

                var data2 = JsonConvert.DeserializeObject<Login_RootObject>(await response.Content.ReadAsStringAsync());

                Database.SetValue("cookie", data2.json.data.cookie);
                Database.SetValue("modhash", data2.json.data.modhash);
                Database.SetValue("username", username);
                Database.SetValue("IsLoggedIn", "true");

                return true;
            }
            catch { return false; }
        }

        public static void Logout()
        {
            Database.RemoveValue("cookie");
            Database.RemoveValue("modhash");
            Database.RemoveValue("username");
            Database.SetValue("IsLoggedIn", "false");
        }

        public static bool IsLoggedIn()
        {
            var ili = Database.GetValue("IsLoggedIn") as string;

            if (ili == "true") 
                return true;

            return false;
        }

        public static string GetCookie()
        {
            return Database.GetValue("cookie") as string;
        }

        public static string GetModhash()
        {
            return Database.GetValue("modhash") as string;
        }

        public static string GetUsername()
        {
            return Database.GetValue("username") as string;
        }
    }
}
