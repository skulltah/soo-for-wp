﻿using Snoo.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Snoo.Reddit.DataTypes.Prefixes;
using System.Threading.Tasks;
using Windows.UI.StartScreen;
using Snoo.Reddit.DataTypes;
using System.Diagnostics;
using SnooHelper;
using Windows.UI.Xaml.Media.Imaging;
using Snoo.UserControls;
using System.Collections.ObjectModel;

namespace Snoo.Pages
{
    public sealed partial class SubredditPage : Page
    {
        private NavigationHelper navigationHelper;
        private BasicPageData pageData = new BasicPageData();

        t5_ subreddit = new t5_();

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public SubredditPage()
        {
            this.InitializeComponent();

            this.DataContext = pageData;

            // Setup the navigation helper
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;

            // Setup the logical page navigation components that allow
            // the page to only show one pane at a time.
            this.navigationHelper.GoBackCommand = new Snoo.Common.RelayCommand(() => this.GoBack(), () => this.CanGoBack());
            this.itemListView.SelectionChanged += itemListView_SelectionChanged;

            // Start listening for Window size changes 
            // to change from showing two panes to showing a single pane
            Window.Current.SizeChanged += Window_SizeChanged;
            this.InvalidateVisualState();
        }

        void itemListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.UsingLogicalPageNavigation())
            {
                this.navigationHelper.GoBackCommand.RaiseCanExecuteChanged();
            }
        }

        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            //pageData.Theme = ThemeManager.GetTheme(ThemeManager.IntToTheme((int)Settings.GetSetting("Theme")));
            //var hexToColourConverter = new HexToColorConverter();
            //this.Background = (SolidColorBrush)hexToColourConverter.Convert(pageData.Theme.Background, typeof(SolidColorBrush), null, null);

            if (e.PageState != null && e.PageState.ContainsKey("subreddit"))
            {
                subreddit = e.PageState["subreddit"] as t5_;

                titlePanel.DataContext = subreddit;

                if (subreddit.UserIsSubscriber)
                    subscribeButton.Label = "unsubscribe";

                if (subreddit.URL == "/")
                {
                    subscribeButton.IsEnabled = false;
                    viewSidebarButton.IsEnabled = false;
                    viewWikiButton.IsEnabled = false;
                }

                if (itemListView.ItemsSource == null)
                    itemListView.ItemsSource = e.PageState["hot"];
            }
            else
            {
                subreddit = e.NavigationParameter as t5_;

                if (subreddit.UserIsSubscriber)
                    subscribeButton.Label = "unsubscribe";

                if (subreddit.URL == "/")
                {
                    subscribeButton.IsEnabled = false;
                    viewSidebarButton.IsEnabled = false;
                    viewWikiButton.IsEnabled = false;
                }

                titlePanel.DataContext = subreddit;

                if (itemListView.ItemsSource == null)
                    itemListView.ItemsSource = (await Reddit.Reddit.GetPosts(subreddit.URL, Reddit.Reddit.SubredditListing.Hot, "", "", "", 0)).Data.Children;


            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            if (!e.PageState.ContainsKey("hot"))
            {
                e.PageState.Add("subreddit", subreddit);
                e.PageState.Add("hot", itemListView.ItemsSource);
            }
            else
            {
                e.PageState["subreddit"] = subreddit;
                e.PageState["hot"] = itemListView.ItemsSource;
            }
        }

        #region Logical page navigation

        // The split page isdesigned so that when the Window does have enough space to show
        // both the list and the dteails, only one pane will be shown at at time.
        //
        // This is all implemented with a single physical page that can represent two logical
        // pages.  The code below achieves this goal without making the user aware of the
        // distinction.

        private const int MinimumWidthForSupportingTwoPanes = 768;

        /// <summary>
        /// Invoked to determine whether the page should act as one logical page or two.
        /// </summary>
        /// <returns>True if the window should show act as one logical page, false
        /// otherwise.</returns>
        private bool UsingLogicalPageNavigation()
        {
            return Window.Current.Bounds.Width < MinimumWidthForSupportingTwoPanes;
        }

        /// <summary>
        /// Invoked with the Window changes size
        /// </summary>
        /// <param name="sender">The current Window</param>
        /// <param name="e">Event data that describes the new size of the Window</param>
        private void Window_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            this.InvalidateVisualState();
        }

        /// <summary>
        /// Invoked when an item within the list is selected.
        /// </summary>
        /// <param name="sender">The GridView displaying the selected item.</param>
        /// <param name="e">Event data that describes how the selection was changed.</param>
        private void ItemListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Invalidate the view state when logical page navigation is in effect, as a change
            // in selection may cause a corresponding change in the current logical page.  When
            // an item is selected this has the effect of changing from displaying the item list
            // to showing the selected item's details.  When the selection is cleared this has the
            // opposite effect.
            if (this.UsingLogicalPageNavigation()) this.InvalidateVisualState();
        }

        private bool CanGoBack()
        {
            if (this.UsingLogicalPageNavigation() && this.itemListView.SelectedItem != null)
            {
                return true;
            }
            else
            {
                return this.navigationHelper.CanGoBack();
            }
        }
        private void GoBack()
        {
            if (this.UsingLogicalPageNavigation() && this.itemListView.SelectedItem != null)
            {
                // When logical page navigation is in effect and there's a selected item that
                // item's details are currently displayed.  Clearing the selection will return to
                // the item list.  From the user's point of view this is a logical backward
                // navigation.
                this.itemListView.SelectedItem = null;
            }
            else
            {
                this.navigationHelper.GoBack();
            }
        }

        private void InvalidateVisualState()
        {
            var visualState = DetermineVisualState();
            VisualStateManager.GoToState(this, visualState, false);
            this.navigationHelper.GoBackCommand.RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Invoked to determine the name of the visual state that corresponds to an application
        /// view state.
        /// </summary>
        /// <returns>The name of the desired visual state.  This is the same as the name of the
        /// view state except when there is a selected item in portrait and snapped views where
        /// this additional logical page is represented by adding a suffix of _Detail.</returns>
        private string DetermineVisualState()
        {
            if (!UsingLogicalPageNavigation())
                return "PrimaryView";

            // Update the back button's enabled state when the view state changes
            var logicalPageBack = this.UsingLogicalPageNavigation() && this.itemListView.SelectedItem != null;

            return logicalPageBack ? "SinglePane_Detail" : "SinglePane";
        }

        #endregion

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private t3_ lastClickedPost = new t3_();

        private async void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            lastClickedPost = (sender as Grid).DataContext as t3_;

            await Task.Delay(100);

            var post = lastClickedPost;

            var lightData = new FlipViewSingleDataItem()
            {
                IsSelf = post.IsSelf,
                Name = post.Name,
                Score = post.Score,
                Thumbnail = post.Thumbnail,
                Title = post.Title,
                URL = post.Url,
                Subreddit = post.Subreddit,
                NumComments = post.NumComments,
                Selftext = post.Selftext
            };

            var data = new PostPageData()
            {
                Post = lightData
            };

            await LoadPostData();
        }

        private async void subscribeButton_Click(object sender, RoutedEventArgs e)
        {
            // un/subscribe

            if (subscribeButton.Label == "subscribe")
            {
                //loadingControl.Show();

                await Reddit.Reddit.Subscribe(subreddit.Name);

                subscribeButton.Label = "unsubscribe";

                //loadingControl.Hide();
            }
            else
            {
                //loadingControl.Show();

                await Reddit.Reddit.Unsubscribe(subreddit.Name);

                subscribeButton.Label = "subscribe";

                //loadingControl.Hide();
            }
        }

        private async void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            // pin to start

            string displayName = subreddit.URL;
            string tileActivationArguments = displayName;

            SecondaryTile secondaryTile = new SecondaryTile(new Guid().ToString(),
                                                displayName,
                                                tileActivationArguments,
                                                new Uri("ms-appx:///Assets/Logo.scale-240.png", UriKind.Absolute),
                                                TileSize.Square150x150);

            await secondaryTile.RequestCreateForSelectionAsync(new Rect(), Windows.UI.Popups.Placement.Default);
        }

        private void viewSidebarButton_Click(object sender, RoutedEventArgs e)
        {
            // view sidebar
        }

        private void viewWikiButton_Click(object sender, RoutedEventArgs e)
        {
            // view wiki
        }

        private void AppBarButton_Click_2(object sender, RoutedEventArgs e)
        {
            // scroll view

            var data = new FlipViewPostData() { posts = itemListView.ItemsSource as ObservableCollection<Posts.Child> };

            this.Frame.Navigate(typeof(ScrollViewPage), data);
        }

        private void AppBarButton_Click_3(object sender, RoutedEventArgs e)
        {
            // new post

           // this.Frame.Navigate(typeof(SubmitPostPage), subreddit.Name.Remove(0, 3));
        }

        private void AppBarButton_Click_4(object sender, RoutedEventArgs e)
        {
            // search

            //this.Frame.Navigate(typeof(SearchPage), subreddit.DisplayName);
        }


        private async Task LoadPostData()
        {
            titleTextBlock.Text = lastClickedPost.Title;
            scoreRun.Text = lastClickedPost.Score.ToString();
            hoursAgoRun.Text = "";
            subredditRun.Text = lastClickedPost.Subreddit;
            numCommentsRun.Text = lastClickedPost.NumComments.ToString();
            urlRun.Text = lastClickedPost.Url;

            //if (lastClickedPost.Likes != null && lastClickedPost.Likes != "")
            //{
            //    if (lastClickedPost.Likes.ToLower() == "true")
            //        upvote.IsChecked = true;
            //    if (lastClickedPost.Likes.ToLower() == "false")
            //        downvote.IsChecked = true;
            //}

            var type = LinkTypeChecker.Check(lastClickedPost.Url);

            switch (type)
            {
                case LinkTypeChecker.LinkType.Picture:
                    imageScrollViewer.Visibility = Visibility.Visible;
                    youtubeGrid.Visibility = Visibility.Collapsed;
                    mediaElement.Visibility = Visibility.Collapsed;
                    albumItemsControl.Visibility = Visibility.Collapsed;
                    webView.Visibility = Visibility.Collapsed;
                    selfTextScrollViewer.Visibility = Visibility.Collapsed;

                    var bmp = new BitmapImage() { CreateOptions = BitmapCreateOptions.None };
                    bmp.UriSource = new Uri(lastClickedPost.Url, UriKind.Absolute);
                    image.Source = bmp;
                    break;
                case LinkTypeChecker.LinkType.GIF:
                    mediaElement.Visibility = Visibility.Visible;
                    imageScrollViewer.Visibility = Visibility.Collapsed;
                    youtubeGrid.Visibility = Visibility.Collapsed;
                    albumItemsControl.Visibility = Visibility.Collapsed;
                    webView.Visibility = Visibility.Collapsed;
                    selfTextScrollViewer.Visibility = Visibility.Collapsed;

                    mediaElement.Source = new Uri(await GFYCat.GetMP4(lastClickedPost.Url), UriKind.Absolute);
                    mediaElement.Play();

                    //saveToPhoneButton.IsEnabled = false;
                    break;
                case LinkTypeChecker.LinkType.Album:
                    albumItemsControl.Visibility = Visibility.Visible;
                    imageScrollViewer.Visibility = Visibility.Collapsed;
                    mediaElement.Visibility = Visibility.Collapsed;
                    youtubeGrid.Visibility = Visibility.Collapsed;
                    webView.Visibility = Visibility.Collapsed;
                    selfTextScrollViewer.Visibility = Visibility.Collapsed;

                    //saveToPhoneButton.IsEnabled = false;
                    break;
                case LinkTypeChecker.LinkType.GFYCat:
                    break;
                case LinkTypeChecker.LinkType.Website:
                    if (!lastClickedPost.IsSelf)
                    {
                        webView.Visibility = Visibility.Visible;
                        imageScrollViewer.Visibility = Visibility.Collapsed;
                        mediaElement.Visibility = Visibility.Collapsed;
                        albumItemsControl.Visibility = Visibility.Collapsed;
                        youtubeGrid.Visibility = Visibility.Collapsed;
                        selfTextScrollViewer.Visibility = Visibility.Collapsed;

                        webView.Navigate(new Uri(lastClickedPost.Url, UriKind.Absolute));

                       // saveToPhoneButton.IsEnabled = false;
                    }
                    else
                    {
                        selfTextScrollViewer.Visibility = Visibility.Visible;

                        selfTextBlock.Text = lastClickedPost.Selftext;

                        //saveToPhoneButton.IsEnabled = false;
                    }
                    break;
                case LinkTypeChecker.LinkType.YouTube:
                    youtubeGrid.Visibility = Visibility.Visible;
                    imageScrollViewer.Visibility = Visibility.Collapsed;
                    mediaElement.Visibility = Visibility.Collapsed;
                    albumItemsControl.Visibility = Visibility.Collapsed;
                    webView.Visibility = Visibility.Collapsed;
                    selfTextScrollViewer.Visibility = Visibility.Collapsed;

                    youtubeWebView.Navigate(new Uri(lastClickedPost.Url, UriKind.Absolute));

                    //saveToPhoneButton.IsEnabled = false;
                    break;
            }

            var data = await Reddit.Reddit.GetComments(lastClickedPost.ID);

            foreach (var c in data)
            {
                foreach (var cc in c.Data.Children)
                {
                    try
                    {
                        //var cC = new CommentItem();
                        //await cC.LoadComment(cc.Data);

                        //itemsControl.Items.Add(cC);
                    }
                    catch { }
                }
            }
        }

        private void mediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            (sender as MediaElement).Play();
        }

        private void Image_ImageOpened(object sender, RoutedEventArgs e)
        {
            image.Width = Window.Current.Bounds.Width;
           // loadingControl.Hide();
        }

        private void webView_ContentLoading(WebView sender, WebViewContentLoadingEventArgs args)
        {
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await Windows.System.Launcher.LaunchUriAsync(new System.Uri("metrotube:VideoPage?VideoID=" + YoutubeHelper.GetIDFromURL(lastClickedPost.Url)));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void TitleRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            
        }
    }
}
